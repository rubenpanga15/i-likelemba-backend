
import com.google.gson.Gson;
import controllers.MembreController;
import daos.DaoAuthentification;
import daos.DaoGeneric;
import daos.DaoMembre;
import entities.Administrateur;
import entities.Arbre;
import entities.CategorieAdhesion;
import entities.Compte;
import entities.Login;
import entities.Membre;
import entities.Parrainage;
import entities.utilities.ArbreAdapter;
import entities.utilities.MembreAdapter;
import java.util.Date;
import java.util.List;
import others.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ghost06
 */
public class TestDaos {

    private static final String TAG = "TEST DAOS";

    public static void main(String... args) {
        try {
            /*MembreController ctrl = new MembreController();
            DaoMembre dao = new DaoMembre();

            Parrainage par = (Parrainage) dao.findById(Parrainage.class, 140);

            ctrl.activerMembre(140, 2);

//            if(ctrl.isIntegration(par)){
//                ctrl.partagerGain(par);
//            }
//            
            Logger.printLog(TAG, ctrl.getError().getErrorDescription());*/
            
            integrer(32,177, "SYS001");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Runtime.getRuntime().exit(0);
        }
    }

    static void integrer(Integer numeroStart, Integer fkParrain, String fkArbre) {

        for (int i = 1; i < 2; i++) {
            MembreAdapter membre = new MembreAdapter();
            membre.setNom("Test");
            membre.setPostnom("Test");
            membre.setPrenom("Test");
            membre.setDateCreat(new Date());
            membre.setTelephone("0000000"+String.valueOf(numeroStart + i));
            membre.setDateNaissance(new Date());
            membre.setFkParrain(fkParrain);
            membre.setLieuNaissance("KIN");
            membre.setSexe("M");
            membre.setFkArbre(fkArbre);
            
            Login login = new Login();
            //login.setRole("membre");
            //login.setToken("XXXxx");
            //login.setUsername(membre.getTelephone());
            login.setPassword("00000000");
            //login.setIsFirstConnection(true);
            //login.setDateCreat(new Date());
            //login.setFkMembre(fkParrain);
            
            membre.setLogin(login);
            
//            MembreController ctrl = new MembreController();
//            ctrl.inscription(membre);
//            
//            Logger.printLog(TAG, ctrl.getError().getErrorDescription());

            Logger.printLog(TAG, new Gson().toJson(membre));
        }
    }

}
