/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author ghost06
 */
public class MouvementCompte {
    
    private Integer id;
    private Integer fkCompte;
    private Integer fkMembre;
    private Double debitUSD;
    private Double creditUSD;
    private Double soldeUSD;
    private Double debitCDF;
    private Double creditCDF;
    private Double soldeCDF;
    private Boolean statut;
    private Date dateCreat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFkCompte() {
        return fkCompte;
    }

    public void setFkCompte(Integer fkCompte) {
        this.fkCompte = fkCompte;
    }

    public Integer getFkMembre() {
        return fkMembre;
    }

    public void setFkMembre(Integer fkMembre) {
        this.fkMembre = fkMembre;
    }

    public Double getDebitUSD() {
        return debitUSD;
    }

    public void setDebitUSD(Double debitUSD) {
        this.debitUSD = debitUSD;
    }

    public Double getCreditUSD() {
        return creditUSD;
    }

    public void setCreditUSD(Double creditUSD) {
        this.creditUSD = creditUSD;
    }

    public Double getSoldeUSD() {
        return soldeUSD;
    }

    public void setSoldeUSD(Double soldeUSD) {
        this.soldeUSD = soldeUSD;
    }

    public Double getDebitCDF() {
        return debitCDF;
    }

    public void setDebitCDF(Double debitCDF) {
        this.debitCDF = debitCDF;
    }

    public Double getCreditCDF() {
        return creditCDF;
    }

    public void setCreditCDF(Double creditCDF) {
        this.creditCDF = creditCDF;
    }

    public Double getSoldeCDF() {
        return soldeCDF;
    }

    public void setSoldeCDF(Double soldeCDF) {
        this.soldeCDF = soldeCDF;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
    
    
}
