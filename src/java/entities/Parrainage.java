/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author Trinix
 */
public class Parrainage {
    
    private Integer id;
    private Integer fkMembre;
    private Integer fkParrain;
    private String fkArbre;
    private Boolean statut;
    private Date dateCreat;
    private Integer fkAgent;
    private Date dateActivation;
    private Boolean isAgent;

    public Boolean getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Boolean isAgent) {
        this.isAgent = isAgent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFkMembre() {
        return fkMembre;
    }

    public void setFkMembre(Integer fkMembre) {
        this.fkMembre = fkMembre;
    }

    public Integer getFkParrain() {
        return fkParrain;
    }

    public void setFkParrain(Integer fkParrain) {
        this.fkParrain = fkParrain;
    }

    public String getFkArbre() {
        return fkArbre;
    }

    public void setFkArbre(String fkArbre) {
        this.fkArbre = fkArbre;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Integer getFkAgent() {
        return fkAgent;
    }

    public void setFkAgent(Integer fkAgent) {
        this.fkAgent = fkAgent;
    }

    public Date getDateActivation() {
        return dateActivation;
    }

    public void setDateActivation(Date dateActivation) {
        this.dateActivation = dateActivation;
    }

    
}
