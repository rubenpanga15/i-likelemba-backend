/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author rubenpanga
 */
public class Paiement {
    
    private Integer id;
    private Integer fkMembre;
    private String fkAdmin;
    private Double montantCDF;
    private Double montantUSD;
    private Date dateDemande;
    private Date datePaiement;
    private Boolean statut;
    private Date dateCreat;
    private Boolean isAgent;
    private Integer fkManager;
    private Double fraisUSD;
    private Double fraisCDF;
    private Double montantTotalUSD;
    private Double montantTotalCDF;
    private String etat;
    
    private Membre membre;
    private Administrateur admin;
    private Membre manager;

    public Double getFraisUSD() {
        return fraisUSD;
    }

    public void setFraisUSD(Double fraisUSD) {
        this.fraisUSD = fraisUSD;
    }

    public Double getFraisCDF() {
        return fraisCDF;
    }

    public void setFraisCDF(Double fraisCDF) {
        this.fraisCDF = fraisCDF;
    }

    public Double getMontantTotalUSD() {
        return montantTotalUSD;
    }

    public void setMontantTotalUSD(Double montantTotalUSD) {
        this.montantTotalUSD = montantTotalUSD;
    }

    public Double getMontantTotalCDF() {
        return montantTotalCDF;
    }

    public void setMontantTotalCDF(Double montantTotalCDF) {
        this.montantTotalCDF = montantTotalCDF;
    }

    public Boolean getIsAgent() {
        return isAgent;
    }

    public void setIsAgent(Boolean isAgent) {
        this.isAgent = isAgent;
    }

    public Integer getFkManager() {
        return fkManager;
    }

    public void setFkManager(Integer fkManager) {
        this.fkManager = fkManager;
    }

    public Membre getManager() {
        return manager;
    }

    public void setManager(Membre manager) {
        this.manager = manager;
    }
    
    

    public Membre getMembre() {
        return membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public Administrateur getAdmin() {
        return admin;
    }

    public void setAdmin(Administrateur admin) {
        this.admin = admin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFkMembre() {
        return fkMembre;
    }

    public void setFkMembre(Integer fkMembre) {
        this.fkMembre = fkMembre;
    }

    public String getFkAdmin() {
        return fkAdmin;
    }

    public void setFkAdmin(String fkAdmin) {
        this.fkAdmin = fkAdmin;
    }

    public Double getMontantCDF() {
        return montantCDF;
    }

    public void setMontantCDF(Double montantCDF) {
        this.montantCDF = montantCDF;
    }

    public Double getMontantUSD() {
        return montantUSD;
    }

    public void setMontantUSD(Double montantUSD) {
        this.montantUSD = montantUSD;
    }

    public Date getDateDemande() {
        return dateDemande;
    }

    public void setDateDemande(Date dateDemande) {
        this.dateDemande = dateDemande;
    }

    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
    
    
}
