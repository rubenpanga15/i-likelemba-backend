/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;
/**
 *
 * @author ghost06
 */

public class Administrateur {
    
    private Integer id;
    private String noms;
    private String username;
    private String password;
    private String token;
    private Integer fkProfilAcces;
    private Boolean statut;
    private Date dateCreat;
    private String code;
    private String role;
    private Double approUSD;
    private Double approCDF;

    public Double getApproUSD() {
        return approUSD;
    }

    public void setApproUSD(Double approUSD) {
        this.approUSD = approUSD;
    }

    public Double getApproCDF() {
        return this.approCDF;
    }

    public void setApproCDF(Double ApproCDF) {
        this.approCDF = ApproCDF;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNoms() {
        return noms;
    }

    public void setNoms(String noms) {
        this.noms = noms;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getFkProfilAcces() {
        return fkProfilAcces;
    }

    public void setFkProfilAcces(Integer fkProfilAcces) {
        this.fkProfilAcces = fkProfilAcces;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
    
    
}
