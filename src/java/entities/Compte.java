/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author ghost06
 */
public class Compte {
    
    private Integer id;
    private Integer fkMembre;
    private Double soldeUSD;
    private Double soldeCDF;
    private Double approUSD;
    private Double approCDF;
    private Boolean statut;
    private Date dateCreat;

    public Double getApproUSD() {
        return approUSD;
    }

    public void setApproUSD(Double approUSD) {
        this.approUSD = approUSD;
    }

    public Double getApproCDF() {
        return approCDF;
    }

    public void setApproCDF(Double approCDF) {
        this.approCDF = approCDF;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFkMembre() {
        return fkMembre;
    }

    public void setFkMembre(Integer fkMembre) {
        this.fkMembre = fkMembre;
    }

    public Double getSoldeUSD() {
        return soldeUSD;
    }

    public void setSoldeUSD(Double soldeUSD) {
        this.soldeUSD = soldeUSD;
    }

    public Double getSoldeCDF() {
        return soldeCDF;
    }

    public void setSoldeCDF(Double soldeCDF) {
        this.soldeCDF = soldeCDF;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
}
