/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;
import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.Columns;

/**
 *
 * @author ghost06
 */
public class Membre {
    private Integer id;
    private String nom;
    private String postnom;
    private String prenom;
    private String sexe;
    private String email;
    private String telephone;
    private Integer fkParrain;
    private String fkArbre;
    private Boolean statut;
    private Date dateCreat;
    private Date dateNaissance;
    private String lieuNaissance;

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPostnom() {
        return postnom;
    }

    public void setPostnom(String postnom) {
        this.postnom = postnom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Integer getFkParrain() {
        return fkParrain;
    }

    public void setFkParrain(Integer fkParrain) {
        this.fkParrain = fkParrain;
    }

    public String getFkArbre() {
        return fkArbre;
    }

    public void setFkArbre(String fkArbre) {
        this.fkArbre = fkArbre;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    
    
}
