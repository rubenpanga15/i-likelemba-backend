/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import entities.utilities.MembreAdapter;
import java.util.Date;

/**
 *
 * @author rubenpanga
 */
public class JournalAppro {
    private Integer id;
    private String type;
    private Integer fkDestinateur;
    private Integer fkProvenance;
    private Integer fkAgent;
    private Integer fkAgentDestinateur;
    private Double montantcdf;
    private Double montantusd;
    private Double soldeCDF;
    private Double soldeUSD;
    private Boolean statut;
    private Date dateCreat;
    
    private Double beneficeUSD;
    private Double beneficeCDF;
    private Double montantTotalCDF;
    private Double montantTotalUSD;
    
    private MembreAdapter destinateur;
    private MembreAdapter provenance;
    private Administrateur agent;
    private Administrateur agentDestinateur;

    public Double getBeneficeUSD() {
        return beneficeUSD;
    }

    public void setBeneficeUSD(Double beneficeUSD) {
        this.beneficeUSD = beneficeUSD;
    }

    public Double getBeneficeCDF() {
        return beneficeCDF;
    }

    public void setBeneficeCDF(Double beneficeCDF) {
        this.beneficeCDF = beneficeCDF;
    }

    public Double getMontantTotalCDF() {
        return montantTotalCDF;
    }

    public void setMontantTotalCDF(Double montantTotalCDF) {
        this.montantTotalCDF = montantTotalCDF;
    }

    public Double getMontantTotalUSD() {
        return montantTotalUSD;
    }

    public void setMontantTotalUSD(Double montantTotalUSD) {
        this.montantTotalUSD = montantTotalUSD;
    }
    
    

    public Administrateur getAgentDestinateur() {
        return agentDestinateur;
    }

    public void setAgentDestinateur(Administrateur agentDestinateur) {
        this.agentDestinateur = agentDestinateur;
    }

    public Integer getFkAgentDestinateur() {
        return fkAgentDestinateur;
    }

    public void setFkAgentDestinateur(Integer fkAgentDestinateur) {
        this.fkAgentDestinateur = fkAgentDestinateur;
    }

    public Integer getFkAgent() {
        return fkAgent;
    }

    public void setFkAgent(Integer fkAgent) {
        this.fkAgent = fkAgent;
    }

    public Administrateur getAgent() {
        return agent;
    }

    public void setAgent(Administrateur agent) {
        this.agent = agent;
    }

    public MembreAdapter getDestinateur() {
        return destinateur;
    }

    public void setDestinateur(MembreAdapter destinateur) {
        this.destinateur = destinateur;
    }

    public MembreAdapter getProvenance() {
        return provenance;
    }

    public void setProvenance(MembreAdapter provenance) {
        this.provenance = provenance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getFkDestinateur() {
        return fkDestinateur;
    }

    public void setFkDestinateur(Integer fkDestinateur) {
        this.fkDestinateur = fkDestinateur;
    }

    public Integer getFkProvenance() {
        return fkProvenance;
    }

    public void setFkProvenance(Integer fkProvenance) {
        this.fkProvenance = fkProvenance;
    }

    public Double getMontantcdf() {
        return montantcdf;
    }

    public void setMontantcdf(Double montantcdf) {
        this.montantcdf = montantcdf;
    }

    public Double getMontantusd() {
        return montantusd;
    }

    public void setMontantusd(Double montantusd) {
        this.montantusd = montantusd;
    }

    public Double getSoldeCDF() {
        return soldeCDF;
    }

    public void setSoldeCDF(Double soldeCDF) {
        this.soldeCDF = soldeCDF;
    }

    public Double getSoldeUSD() {
        return soldeUSD;
    }

    public void setSoldeUSD(Double soldeUSD) {
        this.soldeUSD = soldeUSD;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
    
}
