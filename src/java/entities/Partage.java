/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author Trinix
 */
public class Partage {
    private Integer id;
    private Double g1;
    private Double g2;
    private Double g3;
    private Boolean statut;
    private Date dateCreat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getG1() {
        return g1;
    }

    public void setG1(Double g1) {
        this.g1 = g1;
    }

    public Double getG2() {
        return g2;
    }

    public void setG2(Double g2) {
        this.g2 = g2;
    }

    public Double getG3() {
        return g3;
    }

    public void setG3(Double g3) {
        this.g3 = g3;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
    
}
