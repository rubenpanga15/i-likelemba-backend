/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import entities.Arbre;
import entities.Compte;
import entities.Login;
import entities.Membre;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ghost06
 */
public class MembreAdapter {
    private Integer id;
    private String nom;
    private String postnom;
    private String prenom;
    private String telephone;
    private String email;
    private Integer fkParrain;
    private String fkArbre;
    private Boolean statut;
    private String sexe;
    private Date dateCreat;
    private String lieuNaissance;
    private Date dateNaissance;
    
    private Login login;
    private Compte compte;
    private List<ArbreAdapter> arbres;

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }
    
    

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPostnom() {
        return postnom;
    }

    public void setPostnom(String postnom) {
        this.postnom = postnom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getFkParrain() {
        return fkParrain;
    }

    public void setFkParrain(Integer fkParrain) {
        this.fkParrain = fkParrain;
    }

    public String getFkArbre() {
        return fkArbre;
    }

    public void setFkArbre(String fkArbre) {
        this.fkArbre = fkArbre;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    public List<ArbreAdapter> getArbres() {
        return arbres;
    }

    public void setArbres(List<ArbreAdapter> arbres) {
        this.arbres = arbres;
    }
    
    public Membre convert(){
        Membre membre = new Membre();
        
        membre.setId(this.getId());
        membre.setNom(this.getNom());
        membre.setPostnom(this.getPostnom());
        membre.setPrenom(this.getPrenom());
        membre.setFkParrain(this.getFkParrain());
        membre.setSexe(this.getSexe());
        membre.setEmail((this.getEmail()));
        membre.setTelephone(this.getTelephone());
        membre.setFkArbre(this.getFkArbre());
        membre.setLieuNaissance(this.getLieuNaissance());
        membre.setDateNaissance(this.getDateNaissance());
        
        return membre;
    }

    @Override
    public String toString() {
        return "MembreAdapter{" + "id=" + id + ", nom=" + nom + ", postnom=" + postnom + ", prenom=" + prenom + ", telephone=" + telephone + ", email=" + email + ", fkParrain=" + fkParrain + ", fkArbre=" + fkArbre + ", statut=" + statut + ", sexe=" + sexe + ", dateCreat=" + dateCreat + '}';
    }
    
    
}
