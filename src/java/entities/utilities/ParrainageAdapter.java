/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import entities.Administrateur;
import entities.Membre;
import java.util.Date;

/**
 *
 * @author Trinix
 */
public class ParrainageAdapter {
    private Integer id;
    private MembreAdapter membre;
    private Membre parrain;
    private ArbreAdapter fkArbre;
    private Boolean statut;
    private Date dateCreat;
    private Administrateur agent;
    private Date dateActivation;

    public Date getDateActivation() {
        return dateActivation;
    }

    public void setDateActivation(Date dateActivation) {
        this.dateActivation = dateActivation;
    }
    
    

    public Administrateur getAgent() {
        return agent;
    }

    public void setAgent(Administrateur agent) {
        this.agent = agent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MembreAdapter getMembre() {
        return membre;
    }

    public void setMembre(MembreAdapter membre) {
        this.membre = membre;
    }

    public Membre getParrain() {
        return parrain;
    }

    public void setParrain(Membre parrain) {
        this.parrain = parrain;
    }

    public ArbreAdapter getFkArbre() {
        return fkArbre;
    }

    public void setFkArbre(ArbreAdapter fkArbre) {
        this.fkArbre = fkArbre;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    
    
}
