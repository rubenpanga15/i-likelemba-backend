/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import entities.CategorieAdhesion;
import entities.Devise;
import entities.Membre;
import entities.Partage;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Trinix
 */
public class ArbreAdapter {
    
    private String code;
    private Integer fkGeniteur;
    private Integer fkCategorieAdhesion;
    private String description;
    private Double montant;
    private Boolean statut;
    private Date dateCreat;
    
    private CategorieAdhesion categorieAdhesion;
    private Membre geniteur;
    private Devise devise;
    private Partage partage;
    
    private List<Membre> g1, g2, g3;
    private List<ParrainageAdapter> p1, p2, p3;

    public List<ParrainageAdapter> getP1() {
        return p1;
    }

    public void setP1(List<ParrainageAdapter> p1) {
        this.p1 = p1;
    }

    public List<ParrainageAdapter> getP2() {
        return p2;
    }

    public void setP2(List<ParrainageAdapter> p2) {
        this.p2 = p2;
    }

    public List<ParrainageAdapter> getP3() {
        return p3;
    }

    public void setP3(List<ParrainageAdapter> p3) {
        this.p3 = p3;
    }
    
    

    public Partage getPartage() {
        return partage;
    }

    public void setPartage(Partage partage) {
        this.partage = partage;
    }

    public List<Membre> getG1() {
        return g1;
    }

    public void setG1(List<Membre> g1) {
        this.g1 = g1;
    }

    public List<Membre> getG2() {
        return g2;
    }

    public void setG2(List<Membre> g2) {
        this.g2 = g2;
    }

    public List<Membre> getG3() {
        return g3;
    }

    public void setG3(List<Membre> g3) {
        this.g3 = g3;
    }
    
    

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public Devise getDevise() {
        return devise;
    }

    public void setDevise(Devise devise) {
        this.devise = devise;
    }
    
    

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getFkGeniteur() {
        return fkGeniteur;
    }

    public void setFkGeniteur(Integer fkGeniteur) {
        this.fkGeniteur = fkGeniteur;
    }

    public Integer getFkCategorieAdhesion() {
        return fkCategorieAdhesion;
    }

    public void setFkCategorieAdhesion(Integer fkCategorieAdhesion) {
        this.fkCategorieAdhesion = fkCategorieAdhesion;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public CategorieAdhesion getCategorieAdhesion() {
        return categorieAdhesion;
    }

    public void setCategorieAdhesion(CategorieAdhesion categorieAdhesion) {
        this.categorieAdhesion = categorieAdhesion;
    }

    public Membre getGeniteur() {
        return geniteur;
    }

    public void setGeniteur(Membre geniteur) {
        this.geniteur = geniteur;
    }
    
}
