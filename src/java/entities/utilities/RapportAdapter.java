/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.utilities;

import entities.Paiement;
import java.util.List;

/**
 *
 * @author rubenpanga
 */
public class RapportAdapter {
    public List<ParrainageAdapter> parrainages;
    public List<Paiement> paiements;
    public List<MembreAdapter> membres;

    public List<MembreAdapter> getMembres() {
        return membres;
    }

    public void setMembres(List<MembreAdapter> membres) {
        this.membres = membres;
    }

    public List<ParrainageAdapter> getParrainages() {
        return parrainages;
    }

    public void setParrainages(List<ParrainageAdapter> parrainages) {
        this.parrainages = parrainages;
    }

    public List<Paiement> getPaiements() {
        return paiements;
    }

    public void setPaiements(List<Paiement> paiements) {
        this.paiements = paiements;
    }
    
    
}
