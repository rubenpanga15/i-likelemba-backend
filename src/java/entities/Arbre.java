/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author ghost06
 */
public class Arbre {
    
    private String code;
    private Integer fkGeniteur;
    private Integer fkCategorieAdhesion;
    private Integer fkPartage;
    private Double montant;
    private String description;
    private Boolean statut;
    private Date dateCreat;
    private Integer fkDevise;

    public Integer getFkPartage() {
        return fkPartage;
    }

    public void setFkPartage(Integer fkPartage) {
        this.fkPartage = fkPartage;
    }
    
    

    public Integer getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(Integer fkDevise) {
        this.fkDevise = fkDevise;
    }
    
    

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }
    
    

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getFkGeniteur() {
        return fkGeniteur;
    }

    public void setFkGeniteur(Integer fkGeniteur) {
        this.fkGeniteur = fkGeniteur;
    }

    public Integer getFkCategorieAdhesion() {
        return fkCategorieAdhesion;
    }

    public void setFkCategorieAdhesion(Integer fkCategorieAdhesion) {
        this.fkCategorieAdhesion = fkCategorieAdhesion;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}
