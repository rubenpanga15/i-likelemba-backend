/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author ghost06
 */
public class CategorieAdhesion {
    
    private Integer id;
    private String description;
    private Double montantAdhesion;
    private Boolean statut;
    private Date dateCreat;
    private Integer fkDevise;

    public Integer getFkDevise() {
        return fkDevise;
    }

    public void setFkDevise(Integer fkDevise) {
        this.fkDevise = fkDevise;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getMontantAdhesion() {
        return montantAdhesion;
    }

    public void setMontantAdhesion(Double montantAdhesion) {
        this.montantAdhesion = montantAdhesion;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }
    
    
    
}
