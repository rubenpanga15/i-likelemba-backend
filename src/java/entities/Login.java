/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author ghost06
 */
public class Login {
    
    private Integer id;
    private String username;
    private String password;
    private String token;
    private Integer fkMembre;
    private Boolean statut;
    private Date dateCreat;
    private Boolean isFirstConnection;
    private String role;

    public Boolean getIsFirstConnection() {
        return isFirstConnection;
    }

    public void setIsFirstConnection(Boolean isFirstConnection) {
        this.isFirstConnection = isFirstConnection;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getFkMembre() {
        return fkMembre;
    }

    public void setFkMembre(Integer fkMembre) {
        this.fkMembre = fkMembre;
    }

    public Boolean getStatut() {
        return statut;
    }

    public void setStatut(Boolean statut) {
        this.statut = statut;
    }

    public Date getDateCreat() {
        return dateCreat;
    }

    public void setDateCreat(Date dateCreat) {
        this.dateCreat = dateCreat;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    
}
