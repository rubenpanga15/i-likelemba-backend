/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Trinix
 */
public class Utilities {

    public static String generateRandomString(int digits) {
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        StringBuilder builder = new StringBuilder();
        while (digits-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }

        return builder.toString();
    }
    
    public static Date transformEndDay(Date d) {
        Date date = null;

        Calendar cal = Calendar.getInstance();
        cal.setTime(d);

        cal.add(Calendar.HOUR, 23 - cal.get(Calendar.HOUR_OF_DAY));
        cal.add(Calendar.MINUTE, 59 - cal.get(Calendar.MINUTE));
        cal.add(Calendar.SECOND,59- cal.get(Calendar.SECOND));

        date = cal.getTime();

        return date;
    }

}
