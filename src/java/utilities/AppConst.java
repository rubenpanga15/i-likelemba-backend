/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilities;

/**
 *
 * @author ghost06
 */
public class AppConst {
    
    public static final String ERROR_UNKNOW = "iLikelemba - Erreur inconnue survenue, contatez le support technique.";
    public static final String ERROR_SERVLET_UNKNOW = "iLikelemba - Erreur inconnue au moment du traitement.";
    public static final String NO_PERMISSION = "iLikelemba - Vous n'avez pas droit à effecuter cette action.";
    public static final String PARMS_EMPTY = "iLikelemba - Aucun paramètre reçu.";
    public static final String PARAMS_INCORRECT_FORMAT ="iLikelemba - Paramètres incorrectes.";
    public static final String AGENT_DOESNT_EXIST = "iLikelemba - Votre compte utilisateur n'est relié à aucun agent.";
    public static final String OPERATION_FAILED = "iLikelemba - L'opération n'a pas abouti.";
    public static final String EMPTY_LIST = "iLikelemba Aucune donnée retrouvée.";
    
    public static final String FRAIS_RETRAIT_CODE = "FRAIS_RETRAIT";
    public static final String BENEFICE_MANAGER = "BENEFICE_MANAGER";
    
}
