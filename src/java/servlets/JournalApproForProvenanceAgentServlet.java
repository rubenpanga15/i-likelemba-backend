/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.MembreController;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author rubenpanga
 */
@WebServlet(name = "JournalApproForProvenanceAgentServlet", urlPatterns = {"/api/journal-provenance-agent"})
public class JournalApproForProvenanceAgentServlet extends HttpServlet {
    private static final String TAG= JournalApproForProvenanceAgentServlet.class.getSimpleName();
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            String requestBody = HttpUtilities.getBodyRequest(request);
            Logger.printLog(TAG, requestBody);
            
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            
            String dateDebut = null, dateFin = null, type = null;
            Integer fk = null;
            
            try{
                JSONObject json = new JSONObject(requestBody);
                
                if(json.has("debut"))
                    dateDebut = json.getString("debut");
                
                if(json.has("fin"))
                    dateFin = json.getString("fin");
                
                if(json.has("fk"))
                    fk = json.getInt("fk");
                
                if(json.has("type"))
                    type = json.getString("type");
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            MembreController ctrl = new MembreController();
            if(fk != null)
                httpResponse.setResponse(ctrl.getJournalApproForProvenanceAgent(dateDebut, dateFin, fk));
            else
                httpResponse.setResponse(ctrl.getJournalAppro(dateDebut, dateFin, type));
            httpResponse.setError(ctrl.getError());
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
