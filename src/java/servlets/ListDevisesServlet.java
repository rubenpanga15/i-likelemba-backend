/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.ConfigurationController;
import entities.Devise;
import entities.ErrorResponse;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
@WebServlet(name = "ListDevisesServlet", urlPatterns = {"/api/list-devises"})
public class ListDevisesServlet extends HttpServlet {
    private static final String TAG = ListDevisesServlet.class.getSimpleName();
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "Debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            ConfigurationController ctrl = new ConfigurationController();
            
            List<Devise> devises = ctrl.getListDevises();
            
            httpResponse.setError(ctrl.getError());
            httpResponse.setResponse(devises);
            
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode(ErrorResponse.KO);
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
