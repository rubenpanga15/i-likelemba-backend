/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.MembreController;
import entities.ErrorResponse;
import entities.ValueDataException;
import entities.utilities.ParrainageAdapter;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
@WebServlet(name = "ListParrainageParDateServlet", urlPatterns = {"/api/list-parrainage-date"})
public class ListParrainageParDateServlet extends HttpServlet {
    private static final String TAG = ListParrainageParDateServlet.class.getSimpleName();
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "Debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            String requestBody = HttpUtilities.getBodyRequest(request);
            Logger.printLog(TAG, requestBody);
            
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            
            String debut = null, fin = null;
            
            try{
                JSONObject json  = new JSONObject(requestBody);
                if(json.has("debut"))
                    debut = json.getString("debut");
                if(json.has("fin"))
                    fin = json.getString("fin");
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            MembreController ctrl = new MembreController();
            
            List<ParrainageAdapter> list = ctrl.getListParrainageParDate(debut, fin);
            
            httpResponse.setError(ctrl.getError());
            httpResponse.setResponse(list);
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode(ErrorResponse.KO);
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode(ErrorResponse.KO);
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
