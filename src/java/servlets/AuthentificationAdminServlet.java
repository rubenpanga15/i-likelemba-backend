/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import controllers.AuthentificationController;
import entities.Administrateur;
import entities.ErrorResponse;
import entities.ValueDataException;
import http.HttpDataResponse;
import http.HttpUtilities;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
@WebServlet(name = "AuthentificationAdminServlet", urlPatterns = {"/api/admin-connexion"})
public class AuthentificationAdminServlet extends HttpServlet {
    private static final String  TAG = AuthentificationAdminServlet.class.getSimpleName();
    
    @Override
    public void  doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        Logger.printLog(TAG, "Debut");
        HttpDataResponse httpResponse = new HttpDataResponse();
        
        try{
            String requestBody = HttpUtilities.getBodyRequest(request);
            Logger.printLog(TAG, requestBody);
            
            AppUtilities.controlValue(requestBody, AppConst.PARMS_EMPTY);
            
            String username = null, password = null;
            
            try{
                JSONObject json = new JSONObject(requestBody);
                
                if(json.has("username"))
                    username = json.getString("username");
                if(json.has("password"))
                    password = json.getString("password");
                
            }catch(Exception e){
                throw new ValueDataException(AppConst.PARAMS_INCORRECT_FORMAT);
            }
            
            AuthentificationController ctrl = new AuthentificationController();
            
            Administrateur admin = ctrl.connexion(username, password);
            
            httpResponse.setResponse(admin);
            httpResponse.setError(ctrl.getError());
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode(ErrorResponse.KO);
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode(ErrorResponse.KO);
            httpResponse.getError().setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }finally{
            HttpUtilities.prepareHttpResponse(response, httpResponse).flush();
        }
    }
}
