/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contracts;

import entities.Arbre;
import entities.Membre;
import entities.utilities.ArbreAdapter;
import entities.utilities.MembreAdapter;

/**
 *
 * @author ghost06
 */
public interface IGestionMembre {
    public Membre parrainer(MembreAdapter  membre);
    public ArbreAdapter creerArbre(Arbre arbre);
    
}
