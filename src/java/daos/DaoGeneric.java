/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.QueryParam;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import others.Logger;


import entities.QueryParam;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import others.Logger;

/**
 *
 * @author christianeboma
 */
public class DaoGeneric<T> {
    private static final String TAG = DaoGeneric.class.getSimpleName();
    protected Session session = null;

    protected Session getSession() {
            
        session = null;
        try {
            session = new HibernateSession().getSession();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());
        }
        return session;
    } 

    public T findById(Class classObject, Object id) {
        T object = null;

        session = getSession();
        try {
            
            String sql = "from " + classObject.getSimpleName() + " where id=:id";
            Query query = session.createQuery(sql);
            query.setParameter("id", id);
            object = (T) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());

        } finally {
            if (session != null) {
                session.close();
            }
        }

        return object;
    }
    
    public T findByNumero(Class classObject, String numero){
        T object = null;

        session = getSession();
        try {
            String sql = "from " + classObject.getSimpleName() + " where numero=:code";
            System.out.println(sql);
            Query query = session.createQuery(sql);
            query.setParameter("code", numero);
            object = (T) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());

        } finally {
            if (session != null) {
                session.close();
            }
        }

        return object;
    }
    
    public T findByCode(Class classObject, String code) {
        T object = null;

        session = getSession();
        try {
            String sql = "from " + classObject.getSimpleName() + " where code=:code";
            Query query = session.createQuery(sql);
            query.setParameter("code", code);
            object = (T) query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());

        } finally {
            if (session != null) {
                session.close();
            }
        }

        return object;
    }

    public List<T> getList(Class classObject) {
        session = getSession();
        List<T> list = null;

        try {
            String sql = "from " + classObject.getSimpleName();
            Query query = session.createQuery(sql);
            list = (List<T>) query.list();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());

        } finally {
            if (session != null) {
                session.close();
            }
        }

        return list;
    }

    public List selectSQL(String sql, QueryParam... params) {
        List list = null;

        session = getSession();
        
        try {
            session.beginTransaction();
            Query query = session.createSQLQuery(sql);

            if (params != null) {
                for (QueryParam param : params) {
                    if (param != null) {
                        query.setParameter(param.getAttribut(), param.getValue());
                        try {
                        } catch (Exception e) {
                        }
                    }
                }
            }

            list = query.list();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return list;
    }
    
    public List selectHQL(String hql, int limit, QueryParam... params) {
        List list = null;

        session = getSession();
        try {
            
            Query query = session.createQuery(hql);
            if (params != null) {
                for (QueryParam param : params) {
                    query.setParameter(param.getAttribut(), param.getValue());
                }
            }
            list = query.setMaxResults(limit).list();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return list;
    }

    public List selectHQL(String hql, QueryParam... params) {
        List list = null;

        session = getSession();
        
        try {
            Query query = session.createQuery(hql);
            if (params != null) {
                for (QueryParam param : params) {
                    if(param!=null)
                        query.setParameter(param.getAttribut(), param.getValue());
                }
            }
            list = query.list();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return list;
    }
    
    public List selectHQL(String hql, List<QueryParam> params) {
        List list = null;

        session = getSession();
        
        try {
            Query query = session.createQuery(hql);
            if (params != null) {
                for (QueryParam param : params) {
                    if(param!=null)
                        query.setParameter(param.getAttribut(), param.getValue());
                }
            }
            list = query.list();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return list;
    }
   
    public boolean delete(Class classObject, Object id) {
        boolean success = false;
        session = getSession();

        try {
            String sql = "update " + classObject.getSimpleName() + " set statut = 0 where id=:id";
            Transaction tx = session.beginTransaction();
            Query query = session.createSQLQuery(sql);
            query.setParameter("id", id);
            int linesUpdated = query.executeUpdate();
            tx.commit();

            if (linesUpdated > 0) {
                success = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());

        } finally {
            
            if (session != null) {
                session.close();
            }
        }
        return success;
    }
    
    public boolean deleteEntry(Class classObject, Object id){
        boolean success = false;
        session = getSession();

        try {
            String sql = "DELETE FROM " + classObject.getSimpleName() + " where id=:id";
            Transaction tx = session.beginTransaction();
            Query query = session.createSQLQuery(sql);
            query.setParameter("id", id);
            int linesUpdated = query.executeUpdate();
            tx.commit();

            if (linesUpdated > 0) {
                success = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());

        } finally {
            
            if (session != null) {
                session.close();
            }
        }
        return success;
    }
    
    public String saveReturnCode(T object){
        String id = null;
        
        session = getSession();
        Transaction tx = session.beginTransaction();
        
        try{
            id = (String) session.save(object);
            tx.commit();
        }catch(Exception e){
            e.printStackTrace();
            tx.rollback();
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return id;
    }

    public Integer save(T object){
        Integer id = null;
        
        session = getSession();
        Transaction tx = session.beginTransaction();
        
        try{
            id = (Integer) session.save(object);
            tx.commit();
        }catch(Exception e){
            e.printStackTrace();
            tx.rollback();
        }finally{
            if(session!=null){
                session.close();
            }
        }
        return id;
    }
    
    public boolean saveOrUpdate(T object) {
        boolean success = false;

        session = getSession();
        Transaction tx = session.beginTransaction();

        try {
            session.saveOrUpdate(object);
            tx.commit();
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
            Logger.printLog(TAG, e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return success;
    }

    public boolean executeSQLQuery(String sql, QueryParam... params) {
        boolean succes = false;

        try {
            session = getSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createSQLQuery(sql);
            for (QueryParam param : params) {
                if(param!=null)
                    query.setParameter(param.getAttribut(), param.getValue());
            }
            query.executeUpdate();
            tx.commit();

            succes = true;
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return succes;
    }
    
    public boolean executeSQLQuery(String sql, List<QueryParam> params) {
        boolean succes = false;

        try {
            session = getSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createSQLQuery(sql);
            for (QueryParam param : params) {
                if(param!=null)
                    query.setParameter(param.getAttribut(), param.getValue());
            }
            query.executeUpdate();
            tx.commit();

            succes = true;
        } catch (Exception e) {
            e.printStackTrace();
            Logger.printLog(TAG, e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return succes;
    }
    
    public List<T> convert(Class cl, String names, Query query) {
        List<T> list = new ArrayList<T>();

        try {
            T obj = (T) cl.newInstance();
            

            if (obj != null) {
                List<Object[]> dataSet = query.list();
                String[] alias = names.split(",");
                for(int i=0; i<alias.length; i++){
                    alias[i] = alias[i].trim();
                    alias[i] = alias[i].substring(0,1).toUpperCase() + alias[i].substring(1, alias[i].length());
                    alias[i] = "set"+alias[i];
                }
                
                Method[] methods = obj.getClass().getMethods();
                
                System.out.println("Alias : " + alias.toString());
                System.out.println("Fields : " + methods.toString());

                if (dataSet != null) {
                    for (Object[] row : dataSet) {
                        for (int index = 0; index < alias.length; index++) {
                            
                            
                            for (Method field : methods) {
                                //Class type = field.getType();
                                if (alias[index].equals(field.getName())) {
                                    try{
                                        System.out.println(alias[index]);
                                        System.out.println(field.getName());
                                        //System.out.println(row[index].getClass().getName() + " : " + row[index]);

                                        field.invoke(obj,  row[index]);
                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }
                                    
                                    break;
                                }
                            }
                        }
                        
                        list.add(obj);
                    }
                }
            }

        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } 

        return list;
    }
    
    public List<T> convertV2(Class cl, String names, Query query) {
        List<T> list = new ArrayList<T>();

        try {
            T obj = (T) cl.newInstance();
            

            if (obj != null) {
                List<Object[]> dataSet = query.list();
                String[] alias = names.split(",");
                for(int i=0; i<alias.length; i++){
                    alias[i] = alias[i].trim();
                    alias[i] = alias[i].substring(0,1).toUpperCase() + alias[i].substring(1, alias[i].length());
                    alias[i] = "set"+alias[i];
                }
                
                Method[] methods = obj.getClass().getMethods();
                
                System.out.println("Alias : " + alias.toString());
                System.out.println("Fields : " + methods.toString());

                if (dataSet != null) {
                    for (Object[] row : dataSet) {
                        for (int index = 0; index < alias.length; index++) {
                            
                            
                            for (Method field : methods) {
                                //Class type = field.getType();
                                if (alias[index].equals(field.getName())) {
                                    System.out.println(alias[index]);
                                    System.out.println(field.getName());
                                    System.out.println(row[index].getClass().getName() + " : " + row[index]);
                                    
                                    field.invoke(obj,  row[index]);
                                    
                                    break;
                                }
                            }
                        }
                        
                        list.add(obj);
                    }
                }
            }

        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        }

        return list;
    }
}

