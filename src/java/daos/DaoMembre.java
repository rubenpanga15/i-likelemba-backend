/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.Administrateur;
import entities.Arbre;
import entities.Compte;
import entities.Devise;
import entities.JournalAppro;
import entities.Login;
import entities.Membre;
import entities.Paiement;
import entities.Parrainage;
import entities.Partage;
import entities.QueryParam;
import entities.ValueDataException;
import entities.utilities.ArbreAdapter;
import entities.utilities.MembreAdapter;
import entities.utilities.ParrainageAdapter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import others.Logger;

/**
 *
 * @author Trinix
 */
public class DaoMembre extends DaoGeneric {

    public boolean isValid(Integer fkParrain, String fkArbre) {
        boolean isValid = true;

        String hql = "from Parrainage where fkParrain =:fk and fkArbre =:ar ";

        List<Parrainage> filleuls = this.selectHQL(hql, new QueryParam("fk", fkParrain), new QueryParam("ar", fkArbre));

        if (filleuls != null && filleuls.size() >= 5) {
            isValid = false;
        }

        return isValid;
    }
    
    public Compte getCompteMembre(Integer fkMembre){
        Compte compte = null;
        
        String hql = "from Compte where fkMembre =:fk";
        
        List<Compte> list = this.selectHQL(hql, new QueryParam("fk", fkMembre));
        
        if(list != null || list.size() > 0)
            compte = list.get(0);
        
        return compte;
    }

    public List<ArbreAdapter> getArbresAdapter(Integer fkMembre, String code) {
        List<ArbreAdapter> arbres = null;

        String sql
                = "SELECT\n"
                + "	arbre.`code` code0, \n"
                + "	arbre.fkGeniteur, \n"
                + "	arbre.description desc0, \n"
                + "	arbre.fkCategorieAdhesion , \n"
                + "	arbre.statut stat0, \n"
                + "	arbre.montant, \n"
                + "	devise.id id0, \n"
                + "	devise.`code` code1, \n"
                + "	devise.description desc1, \n"
                + "	devise.statut statu2,\n"
                + "	partage.id idjhkjhjhkj,\n"
                + "	partage.g1 oipoipoipoipoipoipo,\n"
                + "	partage.g2 sdsdssdfd,\n"
                + "	partage.g3 idjhkdfdfffdjhjhkj\n"
                + "FROM\n"
                + "	arbre AS arbre\n"
                + "	INNER JOIN\n"
                + "	devise AS devise\n"
                + "	ON \n"
                + "		arbre.fkDevise = devise.id\n"
                + "	INNER JOIN\n"
                + "	partage AS partage\n"
                + "	ON \n"
                + "		arbre.fkPartage = partage.id\n"
                + "	INNER JOIN\n"
                + "	parrainage AS parrainage\n"
                + "	ON \n"
                + "		arbre.code = parrainage.fkArbre\n"
                + "WHERE\n"
                + "	(parrainage.fkMembre =:fkMembre and parrainage.fkArbre =:code )\n"
                + "	";

        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fkMembre", fkMembre), new QueryParam("code", code));

        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                ArbreAdapter arbre = new ArbreAdapter();

                try {
                    arbre.setCode((String) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setFkGeniteur((Integer) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setDescription((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setFkCategorieAdhesion((Integer) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal big = (BigDecimal) row[5];
                    arbre.setMontant(big.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Devise devise = new Devise();
                try {
                    devise.setId((Integer) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    devise.setCode((String) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    devise.setDescription((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                arbre.setDevise(devise);

                Partage partage = new Partage();
                try {
                    partage.setId((Integer) row[10]);
                } catch (Exception e) {
                }
                try {
                    partage.setG1(((BigDecimal) row[11]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    partage.setG2(((BigDecimal) row[12]).doubleValue());
                } catch (Exception e) {
                }
                try {
                    partage.setG3(((BigDecimal) row[13]).doubleValue());
                } catch (Exception e) {
                }

                try {
                    arbre.setGeniteur((Membre) this.findById(Membre.class, arbre.getFkGeniteur()));
                } catch (Exception e) {
                }

                arbre.setPartage(partage);

                List<Membre> generation1 = this.getFilleuls(arbre.getCode(), fkMembre);
                if (generation1 == null) {
                    generation1 = new ArrayList<>();
                }
                List<Membre> generation2 = new ArrayList<>();
                List<Membre> generation3 = new ArrayList<>();
                arbre.setG1(generation1);

                for (Membre m1 : generation1) {
                    List<Membre> g2 = this.getFilleuls(arbre.getCode(), m1.getId());
                    if (g2 != null) {
                        generation2.addAll(g2);
                    }
                }

                for (Membre m2 : generation2) {
                    List<Membre> g3 = this.getFilleuls(arbre.getCode(), m2.getId());
                    if (g3 != null) {
                        generation3.addAll(g3);
                    }
                }

                arbre.setG1(generation1);
                arbre.setG2(generation2);
                arbre.setG3(generation3);
                
                //parrainages
                
                List<ParrainageAdapter> parrainages1 = this.getListParrainages(fkMembre, code);
                if(parrainages1 == null)
                    parrainages1 = new ArrayList<>();
                
                List<ParrainageAdapter> parrainages2 = new ArrayList<>();
                List<ParrainageAdapter> parrainages3 = new ArrayList<>();
                
                for(ParrainageAdapter p2: parrainages1){
                    List<ParrainageAdapter> parrainages = this.getListParrainages(p2.getMembre().getId(), code);
                    if(parrainages == null) parrainages = new ArrayList<>();
                    parrainages2.addAll(parrainages);
                }
                
                for(ParrainageAdapter p3: parrainages2){
                    List<ParrainageAdapter> parrainages = this.getListParrainages(p3.getMembre().getId(), code);
                    if(parrainages == null) parrainages = new ArrayList<>();
                    parrainages3.addAll(parrainages);
                }
                
                arbre.setP1(parrainages1);
                arbre.setP2(parrainages2);
                arbre.setP3(parrainages3);

                if (arbres == null) {
                    arbres = new ArrayList<>();
                }
                arbres.add(arbre);
            }
        }

        return arbres;
    }

    public List<ArbreAdapter> getArbres() {
        List<ArbreAdapter> arbres = null;

        String sql
                = "SELECT\n"
                + "	arbre.`code` code0, \n"
                + "	arbre.fkGeniteur, \n"
                + "	arbre.description desc0, \n"
                + "	arbre.fkCategorieAdhesion , \n"
                + "	arbre.statut stat0, \n"
                + "	arbre.montant, \n"
                + "	devise.id id0, \n"
                + "	devise.`code` code1, \n"
                + "	devise.description desc1, \n"
                + "	devise.statut statu2,\n"
                + "	partage.id idjhkjhjhkj,\n"
                + "	partage.g1 oipoipoipoipoipoipo,\n"
                + "	partage.g2 sdsdssdfd,\n"
                + "	partage.g3 idjhkdfdfffdjhjhkj\n"
                + "FROM\n"
                + "	arbre AS arbre\n"
                + "	INNER JOIN\n"
                + "	devise AS devise\n"
                + "	ON \n"
                + "		arbre.fkDevise = devise.id\n"
                + "	INNER JOIN\n"
                + "	partage AS partage\n"
                + "	ON \n"
                + "		arbre.fkPartage = partage.id\n"
                + "	";

        List<Object[]> dataset = this.selectSQL(sql);

        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                ArbreAdapter arbre = new ArbreAdapter();

                try {
                    arbre.setCode((String) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setFkGeniteur((Integer) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setDescription((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setFkCategorieAdhesion((Integer) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal big = (BigDecimal) row[5];
                    arbre.setMontant(big.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Devise devise = new Devise();
                try {
                    devise.setId((Integer) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    devise.setCode((String) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    devise.setDescription((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                arbre.setDevise(devise);

                Partage partage = new Partage();
                try {
                    partage.setId((Integer) row[10]);
                } catch (Exception e) {
                }
                try {
                    partage.setG1(((BigDecimal) row[11]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    partage.setG2(((BigDecimal) row[12]).doubleValue());
                } catch (Exception e) {
                }
                try {
                    partage.setG3(((BigDecimal) row[13]).doubleValue());
                } catch (Exception e) {
                }
                try {
                    arbre.setGeniteur((Membre) this.findById(Membre.class, arbre.getFkGeniteur()));
                } catch (Exception e) {
                }

                arbre.setPartage(partage);

                if (arbres == null) {
                    arbres = new ArrayList<>();
                }
                arbres.add(arbre);
            }
        }

        return arbres;
    }

    public List<ArbreAdapter> getArbresAdapter(Integer fkMembre) {
        List<ArbreAdapter> arbres = null;

        String sql
                = "SELECT\n"
                + "	arbre.`code` code0, \n"
                + "	arbre.fkGeniteur, \n"
                + "	arbre.description desc0, \n"
                + "	arbre.fkCategorieAdhesion , \n"
                + "	arbre.statut stat0, \n"
                + "	arbre.montant, \n"
                + "	devise.id id0, \n"
                + "	devise.`code` code1, \n"
                + "	devise.description desc1, \n"
                + "	devise.statut statu2,\n"
                + "	partage.id idjhkjhjhkj,\n"
                + "	partage.g1 oipoipoipoipoipoipo,\n"
                + "	partage.g2 sdsdssdfd,\n"
                + "	partage.g3 idjhkdfdfffdjhjhkj\n"
                + "FROM\n"
                + "	arbre AS arbre\n"
                + "	INNER JOIN\n"
                + "	devise AS devise\n"
                + "	ON \n"
                + "		arbre.fkDevise = devise.id\n"
                + "	INNER JOIN\n"
                + "	partage AS partage\n"
                + "	ON \n"
                + "		arbre.fkPartage = partage.id\n"
                + "	INNER JOIN\n"
                + "	parrainage AS parrainage\n"
                + "	ON \n"
                + "		arbre.code = parrainage.fkArbre\n"
                + "WHERE\n"
                + "	(parrainage.fkMembre =:fkMembre)\n"
                + "	";

        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fkMembre", fkMembre));

        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                ArbreAdapter arbre = new ArbreAdapter();

                try {
                    arbre.setCode((String) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setFkGeniteur((Integer) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setDescription((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setFkCategorieAdhesion((Integer) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal big = (BigDecimal) row[5];
                    arbre.setMontant(big.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Devise devise = new Devise();
                try {
                    devise.setId((Integer) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    devise.setCode((String) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    devise.setDescription((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                arbre.setDevise(devise);

                Partage partage = new Partage();
                try {
                    partage.setId((Integer) row[10]);
                } catch (Exception e) {
                }
                try {
                    partage.setG1(((BigDecimal) row[11]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    partage.setG2(((BigDecimal) row[12]).doubleValue());
                } catch (Exception e) {
                }
                try {
                    partage.setG3(((BigDecimal) row[13]).doubleValue());
                } catch (Exception e) {
                }

                try {
                    arbre.setGeniteur((Membre) this.findById(Membre.class, arbre.getFkGeniteur()));
                } catch (Exception e) {
                }

                arbre.setPartage(partage);

                List<Membre> generation1 = this.getFilleuls(arbre.getCode(), fkMembre);
                if (generation1 == null) {
                    generation1 = new ArrayList<>();
                }
                List<Membre> generation2 = new ArrayList<>();
                List<Membre> generation3 = new ArrayList<>();
                arbre.setG1(generation1);

                for (Membre m1 : generation1) {
                    List<Membre> g2 = this.getFilleuls(arbre.getCode(), m1.getId());
                    if (g2 != null) {
                        generation2.addAll(g2);
                    }
                }

                for (Membre m2 : generation2) {
                    List<Membre> g3 = this.getFilleuls(arbre.getCode(), m2.getId());
                    if (g3 != null) {
                        generation3.addAll(g3);
                    }
                }

                arbre.setG1(generation1);
                arbre.setG2(generation2);
                arbre.setG3(generation3);
                
                //parrainages
                
                List<ParrainageAdapter> parrainages1 = this.getListParrainages(fkMembre, arbre.getCode());
                if(parrainages1 == null)
                    parrainages1 = new ArrayList<>();
                
                List<ParrainageAdapter> parrainages2 = new ArrayList<>();
                List<ParrainageAdapter> parrainages3 = new ArrayList<>();
                
                for(ParrainageAdapter p2: parrainages1){
                    List<ParrainageAdapter> parrainages = this.getListParrainages(p2.getMembre().getId(), arbre.getCode());
                    if(parrainages == null) parrainages = new ArrayList<>();
                    parrainages2.addAll(parrainages);
                }
                
                for(ParrainageAdapter p3: parrainages2){
                    List<ParrainageAdapter> parrainages = this.getListParrainages(p3.getMembre().getId(), arbre.getCode());
                    if(parrainages == null) parrainages = new ArrayList<>();
                    parrainages3.addAll(parrainages);
                }
                
                arbre.setP1(parrainages1);
                arbre.setP2(parrainages2);
                arbre.setP3(parrainages3);

                if (arbres == null) {
                    arbres = new ArrayList<>();
                }
                arbres.add(arbre);
            }
        }

        return arbres;
    }

    public ArbreAdapter getAbreAdapter(String code) {
        ArbreAdapter arbre = null;

        String sql
                = "SELECT\n"
                + "	arbre.`code` code0, \n"
                + "	arbre.fkGeniteur, \n"
                + "	arbre.description desc0, \n"
                + "	arbre.fkCategorieAdhesion , \n"
                + "	arbre.statut stat0, \n"
                + "	arbre.montant, \n"
                + "	devise.id id0, \n"
                + "	devise.`code` code1, \n"
                + "	devise.description desc1, \n"
                + "	devise.statut statu2,\n"
                + "	partage.id idjhkjhjhkj,\n"
                + "	partage.g1 oipoipoipoipoipoipo,\n"
                + "	partage.g2 sdsdssdfd,\n"
                + "	partage.g3 idjhkdfdfffdjhjhkj\n"
                + "FROM\n"
                + "	arbre AS arbre\n"
                + "	INNER JOIN\n"
                + "	devise AS devise\n"
                + "	ON \n"
                + "		arbre.fkDevise = devise.id\n"
                + "	INNER JOIN\n"
                + "	partage AS partage\n"
                + "	ON \n"
                + "		arbre.fkPartage = partage.id\n"
                + "WHERE\n"
                + "	arbre.`code` =:code\n"
                + "	";

        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("code", code));

        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                arbre = new ArbreAdapter();

                try {
                    arbre.setCode((String) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setFkGeniteur((Integer) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setDescription((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    arbre.setFkCategorieAdhesion((Integer) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal big = (BigDecimal) row[5];
                    arbre.setMontant(big.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Devise devise = new Devise();
                try {
                    devise.setId((Integer) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    devise.setCode((String) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    devise.setDescription((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    arbre.setGeniteur((Membre) this.findById(Membre.class, arbre.getFkGeniteur()));
                } catch (Exception e) {
                }

                arbre.setDevise(devise);

                Partage partage = new Partage();
                try {
                    partage.setId((Integer) row[10]);
                } catch (Exception e) {
                }
                try {
                    partage.setG1(((BigDecimal) row[11]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    partage.setG2(((BigDecimal) row[12]).doubleValue());
                } catch (Exception e) {
                }
                try {
                    partage.setG3(((BigDecimal) row[13]).doubleValue());
                } catch (Exception e) {
                }

                arbre.setPartage(partage);

                List<Membre> generation1 = this.getFilleuls(arbre.getCode(), arbre.getFkGeniteur());
                if (generation1 == null) {
                    generation1 = new ArrayList<>();
                }
                List<Membre> generation2 = new ArrayList<>();
                List<Membre> generation3 = new ArrayList<>();
                arbre.setG1(generation1);

                for (Membre m1 : generation1) {
                    List<Membre> g2 = this.getFilleuls(arbre.getCode(), m1.getId());
                    if (g2 != null) {
                        generation2.addAll(g2);
                    }
                }

                for (Membre m2 : generation2) {
                    List<Membre> g3 = this.getFilleuls(arbre.getCode(), m2.getId());
                    if (g3 != null) {
                        generation3.addAll(g3);
                    }
                }

                arbre.setG1(generation1);
                arbre.setG2(generation2);
                arbre.setG3(generation3);
            }
        }

        return arbre;
    }

    public List<Membre> getFilleuls(String fkArbre, Integer fkParrain) {
        List<Membre> membres = null;

        String sql
                = "SELECT\n"
                + "membre.id,\n"
                + "membre.nom,\n"
                + "membre.postnom,\n"
                + "membre.prenom,\n"
                + "membre.sexe,\n"
                + "membre.email,\n"
                + "membre.telephone,\n"
                + "membre.fkParrain,\n"
                + "membre.fkArbre,\n"
                + "membre.statut,\n"
                + "membre.dateCreat,\n"
                + "membre.lieuNaissance,\n"
                + "membre.dateNaissance\n"
                + "FROM\n"
                + "membre AS membre\n"
                + "Inner Join parrainage AS parrainage ON membre.id = parrainage.fkMembre\n"
                + "WHERE\n"
                + "parrainage.fkParrain =  :fkParrain AND\n"
                + "parrainage.fkArbre =  :fkArbre";

        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fkParrain", fkParrain), new QueryParam("fkArbre", fkArbre));

        if (dataset != null) {
            for (Object[] row : dataset) {
                Membre membre = new Membre();

                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                }
                try {
                    membre.setStatut((Boolean) row[9]);
                } catch (Exception e) {
                }
                try {
                    membre.setDateCreat((Date) row[10]);
                } catch (Exception e) {
                }
                try {
                    membre.setLieuNaissance((String) row[11]);
                } catch (Exception e) {
                }
                try {
                    membre.setDateNaissance((Date) row[12]);
                } catch (Exception e) {
                }

                if (membres == null) {
                    membres = new ArrayList<>();
                }

                membres.add(membre);
            }
        }

        return membres;
    }

    public ArbreAdapter createArbre(Integer fkGeniteur, Integer fkPartage, Double montant, String description, Integer fkDevise) {
        ArbreAdapter arbre = null;

        String sql = "call create_arbre(:fkGeniteur, :fkPartage, :montant, :description, :fkDevise)";

        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fkGeniteur", fkGeniteur), new QueryParam("fkPartage", fkPartage), new QueryParam("montant", montant), new QueryParam("fkDevise", fkDevise), new QueryParam("description", description));
        String code = null;
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                try {
                    code = (String) row[0];
                } catch (Exception e) {
                }
            }
        }

        arbre = this.getAbreAdapter(code);

        return arbre;
    }

    public boolean activerMembre(String telephone) {
        boolean activated = false;

        String sql = "UPDATE membre set statut = 1 where telephone = '" + telephone + "'";

        activated = this.executeSQLQuery(sql);

        return activated;
    }

    public boolean isAllArbreCorrect(Integer fkMembre) {
        boolean isCorrect = true;

        String sql = "select count(*) from parrainage where fkParrain =:fk group by fkArbre";
        String sql2 = "select distinct(fkArbre) from parrainage where fkMembre =:fk";
        String sql3 = "select fkMembre from parrainage where fkParrain =:fk group by fkArbre";

        List<BigInteger> list = this.selectSQL(sql, new QueryParam("fk", fkMembre));
        List<Parrainage> list2 = this.selectHQL("from Parrainage where fkParrain =:fk", new QueryParam("fk", fkMembre));
        
        

        /*
            Recuperation de toutes les branches
            
         */
        List<ArbreAdapter> arbres = this.getArbresAdapter(fkMembre);

        if (list == null || list != null && list.size() <= 0) {
            isCorrect = false;
        }

        if (list != null && list.size() > 0) {
            int compte = 0;
            for (BigInteger count : list) {
                if (count.intValue() < 5 || count == null) {

                    isCorrect = false;
                    break;
                }
                compte++;
            }

            if (compte != list2.size()) {
                isCorrect = false;
            }
        }

        return isCorrect;
    }
    
    public boolean isAllArbreCorrect2(Integer fkMembre) {
        boolean isCorrect = true;

        String sql2 = "select distinct(fkArbre) from parrainage where fkMembre =:fk ";
        
        List<String> arbres = this.selectSQL(sql2, new QueryParam("fk", fkMembre));
        
        if(arbres != null && arbres.size() > 0){
            for(String fkArbre: arbres){
                List<Parrainage> parrainages = this.getListParrainge(fkMembre, fkArbre);
                if(parrainages == null || parrainages.size() <= 0){
                    isCorrect = false;
                    break;
                }
                for(Parrainage parrainage: parrainages){
                    if(!this.isIntegeration(parrainage.getFkMembre(), fkArbre)){
                        isCorrect = false;
                        break;
                    }
                }
                if(!isCorrect)
                    break;
            }
        }

        return isCorrect;
    }
    
    public List<Parrainage> getListParrainge(Integer fkParrain, String fkArbre){
        List<Parrainage> liste = null;
        
        String hql = "from Parrainage where fkParrain =:fk and fkArbre =:fkArbre";
        
        liste = this.selectHQL(hql, new QueryParam("fk", fkParrain), new QueryParam("fkArbre", fkArbre));
        
        return liste;
    }
    
    public boolean isIntegeration(Integer fkMembre, String fkArbre){
        boolean correct = false;
        
        /*String sql = "select count(*) from parrainage where fkParrain =:fk and fkArbre =:arbre and statut = 1";
        
        List<BigInteger> list = this.selectSQL(sql, new QueryParam("fk", fkMembre), new QueryParam("arbre", fkArbre));
        
        if (list != null && list.size() > 0) {
            BigInteger bigCount = list.get(0);
            int count  = bigCount.intValue();
            Logger.printLog("INEGRATION", count + "");
            if(count >= 5)
                correct = true;
        }*/
        
        String hql = "from Parrainage where fkParrain =:fk and fkArbre =:arbre and fkAgent is not null";
        
        List<Parrainage> list = this.selectHQL(hql, new QueryParam("fk", fkMembre), new QueryParam("arbre", fkArbre));
        
        if(list != null && list.size() >= 5)
            correct = true;
        
        
        return correct;
    }

    public Integer saveMembre(Membre membre) {
        Integer id = null;

        boolean existe = false;

        String hql = "from Membre where telephone =:tel";

        List<Membre> list = this.selectHQL(hql, new QueryParam("tel", membre.getTelephone()));
        if (list != null && list.size() > 0) {
            existe = true;
        }

        if (!existe) {
            id = this.save(membre);
            Login login = new Login();
            login.setUsername(membre.getTelephone());
            login.setFkMembre(id);
            login.setDateCreat(new Date());
            //login.setPassword(utilities.Utilities.generateRandomString(6));
            //login.setPassword("0000");
            login.setRole("membre");
            login.setToken(utilities.Utilities.generateRandomString(28));
            login.setStatut(true);
            login.setIsFirstConnection(true);
            this.save(login);
        } else {
            id = list.get(0).getId();
        }

        return id;
    }

    public Integer saveMembre(Membre membre, Login login) {
        Integer id = null;

        boolean existe = false;

        String hql = "from Membre where telephone =:tel";

        List<Membre> list = this.selectHQL(hql, new QueryParam("tel", membre.getTelephone()));
        if (list != null && list.size() > 0) {
            existe = true;
        }

        if (!existe) {
            id = this.save(membre);
            login.setUsername(membre.getTelephone());
            login.setFkMembre(id);
            login.setDateCreat(new Date());
            //login.setPassword(utilities.Utilities.generateRandomString(6));
            //login.setPassword("0000");
            login.setToken(utilities.Utilities.generateRandomString(28));
            login.setStatut(true);
            login.setRole("membre");
            login.setIsFirstConnection(true);
            this.save(login);
        } else {
            id = list.get(0).getId();
            Parrainage parrainage = new Parrainage();
            parrainage.setDateCreat(new Date());
            parrainage.setFkArbre(membre.getFkArbre());
            parrainage.setFkMembre(id);
            parrainage.setFkParrain(membre.getFkParrain());
            parrainage.setStatut(false);
            parrainage.setIsAgent(true);
            
            if(this.save(parrainage) == null)
                id = null;
        }

        return id;
    }

    public boolean existInBranche(String telephone, String fkAbre) {
        boolean exist = false;

        String hql = "select * from parrainage where fkMembre = (select id from membre where telephone =:tel) and fkArbre =:fkArbre";

        List<Object[]> list = this.selectSQL(hql, new QueryParam("tel", telephone), new QueryParam("fkArbre", fkAbre));

        if (list != null && list.size() > 0) {
            exist = true;
        }

        return exist;
    }

    public List<MembreAdapter> getListMembre(String motclef) {
        List<MembreAdapter> list = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	compte.approUSD,\n"
                + "	compte.approCDF\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "WHERE\n"
                + "	membre.telephone like '%" + motclef + "%' OR\n"
                + "	membre.nom like '%" + motclef + "%' OR\n"
                + "	membre.postnom like '%" + motclef + "%' OR\n"
                + "	membre.prenom like '%" + motclef + "%' \n";
        List<Object[]> dataset = this.selectSQL(sql);
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                MembreAdapter membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try {
                    BigDecimal bd = (BigDecimal) row[16];
                    compte.setApproUSD(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try {
                    BigDecimal bd = (BigDecimal) row[17];
                    compte.setApproCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

                if (list == null) {
                    list = new ArrayList<>();
                }

                list.add(membre);

            }
        }

        return list;
    }
    
    public List<MembreAdapter> getListMembre(Date debut, Date fin) {
        List<MembreAdapter> list = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	compte.approUSD,\n"
                + "	compte.approCDF\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "WHERE\n"
                + "	membre.dateCreat between :debut and :fin";
        
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("debut", debut), new QueryParam("fin", fin));
        
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                MembreAdapter membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[16];
                    compte.setApproUSD(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[17];
                    compte.setApproCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

                if (list == null) {
                    list = new ArrayList<>();
                }

                list.add(membre);

            }
        }

        return list;
    }
    
    public List<MembreAdapter> getAllMembre() {
        List<MembreAdapter> list = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	compte.approUSD,\n"
                + "	compte.approCDF\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n";
        List<Object[]> dataset = this.selectSQL(sql);
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                MembreAdapter membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[16];
                    compte.setApproUSD(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[17];
                    compte.setApproCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

                if (list == null) {
                    list = new ArrayList<>();
                }

                list.add(membre);

            }
        }

        return list;
    }

    public List<ParrainageAdapter> getListParrainages(String motclef) {
        List<ParrainageAdapter> list = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	parrainage.id id34,\n"
                + "	parrainage.fkArbre fkopop,\n"
                + "	parrainage.statut st809,\n"
                + "	parrainage.dateCreat date809,\n"
                + "	parrainage.fkAgent hshqqhqskdh,\n"
                + "	parrainage.fkParrain gfgfhfghfgg,\n"
                + "	compte.approUSD,\n"
                + "	compte.approCDF\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "	INNER JOIN\n"
                + "	parrainage AS parrainage\n"
                + "	ON \n"
                + "		membre.id = parrainage.fkMembre\n"
                + "WHERE\n"
                + "	membre.telephone like '%" + motclef + "%' OR\n"
                + "	membre.nom like '%" + motclef + "%' OR\n"
                + "	membre.postnom like '%" + motclef + "%' OR\n"
                + "	membre.prenom like '%" + motclef + "%'\n";
        List<Object[]> dataset = this.selectSQL(sql);
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                ParrainageAdapter parrainage = new ParrainageAdapter();
                MembreAdapter membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try {
                    BigDecimal bd = (BigDecimal) row[22];
                    compte.setApproUSD(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try {
                    BigDecimal bd = (BigDecimal) row[23];
                    compte.setApproCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

                parrainage.setMembre(membre);

                try {
                    parrainage.setId((Integer) row[16]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setFkArbre(this.getAbreAdapter((String) row[17]));
                } catch (Exception e) {
                }

                try {
                    parrainage.setStatut((Boolean) row[18]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setDateCreat((Date) row[19]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setAgent((Administrateur) this.findById(Administrateur.class, (Integer) row[20]));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setParrain((Membre) this.findById(Membre.class, (Integer) row[21]));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (list == null) {
                    list = new ArrayList<>();
                }
                list.add(parrainage);
            }
        }

        return list;
    }

    public List<ParrainageAdapter> getListParrainages(Date debut, Date fin) {
        List<ParrainageAdapter> list = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	parrainage.id id34,\n"
                + "	parrainage.fkArbre fkopop,\n"
                + "	parrainage.statut st809,\n"
                + "	parrainage.dateCreat date809,\n"
                + "	parrainage.fkAgent hshqqhqskdh,\n"
                + "	parrainage.fkParrain gfgggfhghgfhfghfgg,\n"
                + "	parrainage.dateActivation gfghgfhfghfgg\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "	INNER JOIN\n"
                + "	parrainage AS parrainage\n"
                + "	ON \n"
                + "		membre.id = parrainage.fkMembre\n"
                + "WHERE\n"
                + "	parrainage.dateCreat between :debut and :fin\n";
        ;
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("debut", debut), new QueryParam("fin", fin));
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                ParrainageAdapter parrainage = new ParrainageAdapter();
                MembreAdapter membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

                parrainage.setMembre(membre);

                try {
                    parrainage.setId((Integer) row[16]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setFkArbre(this.getAbreAdapter((String) row[17]));
                } catch (Exception e) {
                }

                try {
                    parrainage.setAgent((Administrateur) this.findById(Administrateur.class, (Integer) row[20]));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setStatut((Boolean) row[18]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setDateCreat((Date) row[19]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setParrain((Membre) this.findById(Membre.class, (Integer) row[21]));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try{
                    parrainage.setDateActivation((Date) row[22]);
                }catch(Exception e){}

                if (list == null) {
                    list = new ArrayList<>();
                }
                list.add(parrainage);
            }
        }

        return list;
    }
    
    public List<ParrainageAdapter> getListParrainagesActivated(Date debut, Date fin) {
        List<ParrainageAdapter> list = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	parrainage.id id34,\n"
                + "	parrainage.fkArbre fkopop,\n"
                + "	parrainage.statut st809,\n"
                + "	parrainage.dateCreat date809,\n"
                + "	parrainage.fkAgent hshqqhqskdh,\n"
                + "	parrainage.fkParrain gfgggfhghgfhfghfgg,\n"
                + "	parrainage.dateActivation gfgggfhghfgg\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "	INNER JOIN\n"
                + "	parrainage AS parrainage\n"
                + "	ON \n"
                + "		membre.id = parrainage.fkMembre\n"
                + "WHERE\n"
                + "	parrainage.dateActivation is not null and parrainage.dateActivation between :debut and :fin\n";
        ;
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("debut", debut), new QueryParam("fin", fin));
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                ParrainageAdapter parrainage = new ParrainageAdapter();
                MembreAdapter membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

                parrainage.setMembre(membre);

                try {
                    parrainage.setId((Integer) row[16]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setFkArbre(this.getAbreAdapter((String) row[17]));
                } catch (Exception e) {
                }

                try {
                    parrainage.setAgent((Administrateur) this.findById(Administrateur.class, (Integer) row[20]));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setStatut((Boolean) row[18]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setDateCreat((Date) row[19]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setParrain((Membre) this.findById(Membre.class, (Integer) row[21]));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try{
                    parrainage.setDateActivation((Date) row[22]);
                }catch(Exception e){}

                if (list == null) {
                    list = new ArrayList<>();
                }
                list.add(parrainage);
            }
        }

        return list;
    }
    
    public List<ParrainageAdapter> getListParrainagesActivatedForManager(Date debut, Date fin, Integer fkManager) {
        List<ParrainageAdapter> list = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	parrainage.id id34,\n"
                + "	parrainage.fkArbre fkopop,\n"
                + "	parrainage.statut st809,\n"
                + "	parrainage.dateCreat date809,\n"
                + "	parrainage.fkAgent hshqqhqskdh,\n"
                + "	parrainage.fkParrain gfgggfhghgfhfghfgg,\n"
                + "	parrainage.dateActivation gfgggfhghfgg\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "	INNER JOIN\n"
                + "	parrainage AS parrainage\n"
                + "	ON \n"
                + "		membre.id = parrainage.fkMembre\n"
                + "WHERE\n"
                + "	parrainage.fkAgent =:fk and parrainage.isAgent = 0 and  parrainage.dateActivation is not null and parrainage.dateActivation between :debut and :fin order by dateActivation desc\n";
        ;
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("debut", debut), new QueryParam("fin", fin), new QueryParam("fk", fkManager));
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                ParrainageAdapter parrainage = new ParrainageAdapter();
                MembreAdapter membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

                parrainage.setMembre(membre);

                try {
                    parrainage.setId((Integer) row[16]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setFkArbre(this.getAbreAdapter((String) row[17]));
                } catch (Exception e) {
                }

                try {
                    parrainage.setAgent((Administrateur) this.findById(Administrateur.class, (Integer) row[20]));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setStatut((Boolean) row[18]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setDateCreat((Date) row[19]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setParrain((Membre) this.findById(Membre.class, (Integer) row[21]));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try{
                    parrainage.setDateActivation((Date) row[22]);
                }catch(Exception e){}

                if (list == null) {
                    list = new ArrayList<>();
                }
                list.add(parrainage);
            }
        }

        return list;
    }
    
    public List<ParrainageAdapter> getListParrainages(Integer fkMembre, String fkArbre) {
        List<ParrainageAdapter> list = null;
        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	parrainage.id id34,\n"
                + "	parrainage.fkArbre fkopop,\n"
                + "	parrainage.statut st809,\n"
                + "	parrainage.dateCreat date809,\n"
                + "	parrainage.fkAgent hshqqhqskdh,\n"
                + "	parrainage.fkParrain gfgggfhghgfhfghfgg,\n"
                + "	parrainage.dateActivation gfgggfhghfgg\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "	INNER JOIN\n"
                + "	parrainage AS parrainage\n"
                + "	ON \n"
                + "		membre.id = parrainage.fkMembre\n"
                + "WHERE\n"
                + "	parrainage.fkParrain =:fk and  parrainage.fkArbre =:arbre \n";
        ;
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fk", fkMembre), new QueryParam("arbre", fkArbre));
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                ParrainageAdapter parrainage = new ParrainageAdapter();
                MembreAdapter membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

                parrainage.setMembre(membre);

                try {
                    parrainage.setId((Integer) row[16]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setFkArbre(this.getAbreAdapter((String) row[17]));
                } catch (Exception e) {
                }

                try {
                    parrainage.setAgent((Administrateur) this.findById(Administrateur.class, (Integer) row[20]));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setStatut((Boolean) row[18]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setDateCreat((Date) row[19]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setParrain((Membre) this.findById(Membre.class, (Integer) row[21]));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try{
                    parrainage.setDateActivation((Date) row[22]);
                }catch(Exception e){}

                if (list == null) {
                    list = new ArrayList<>();
                }
                list.add(parrainage);
            }
        }

        return list;
    }

    public List<ParrainageAdapter> getListParrainages(Date debut, Date fin, Integer fkAgent) {
        List<ParrainageAdapter> list = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	parrainage.id id34,\n"
                + "	parrainage.fkArbre fkopop,\n"
                + "	parrainage.statut st809,\n"
                + "	parrainage.dateCreat date809,\n"
                + "	parrainage.fkAgent hshqqhqskdh,\n"
                + "	parrainage.fkParrain gfgggfhghgfhfghfgg,\n"
                + "	parrainage.dateActivation gfgggfhghgfhfghfkmlklmklgg\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "	INNER JOIN\n"
                + "	parrainage AS parrainage\n"
                + "	ON \n"
                + "		membre.id = parrainage.fkMembre\n"
                + "WHERE\n"
                + "	(parrainage.dateActivation between :debut and :fin) and fkAgent =:fkAgent\n";
        ;
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("debut", debut), new QueryParam("fin", fin), new QueryParam("fkAgent", fkAgent));
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                ParrainageAdapter parrainage = new ParrainageAdapter();
                MembreAdapter membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

                parrainage.setMembre(membre);

                try {
                    parrainage.setId((Integer) row[16]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setFkArbre(this.getAbreAdapter((String) row[17]));
                } catch (Exception e) {
                }

                try {
                    parrainage.setAgent((Administrateur) this.findById(Administrateur.class, (Integer) row[20]));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setStatut((Boolean) row[18]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setDateCreat((Date) row[19]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setParrain((Membre) this.findById(Membre.class, (Integer) row[21]));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    parrainage.setDateActivation((Date) row[22]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (list == null) {
                    list = new ArrayList<>();
                }
                list.add(parrainage);
            }
        }

        return list;
    }

    public boolean activatedMembre(Integer id, Integer fkAgent) {
        boolean activated = false;

        String sql
                = "update parrainage set statut = 1, dateActivation= now(), fkAgent = " + fkAgent + " where id = " + id;

        activated = this.executeSQLQuery(sql);

        return activated;
    }

    public boolean isActivatedInBranche(Integer fkMembre, String fkArbre) {
        boolean isActivated = false;
        String hql = "from Parrainage where fkMembre =:fkM and fkArbre =:fkArbre";

        List<Parrainage> list = this.selectHQL(hql, 1, new QueryParam("fkM", fkMembre), new QueryParam("fkArbre", fkArbre));

        if (list != null && list.size() > 0 && list.get(0).getStatut()) {
            isActivated = true;
        }

        return isActivated;
    }

    public MembreAdapter getMembre(Integer id) {
        MembreAdapter membre = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	'XXXXXXX',\n"
                + "	'XXXXX' ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	login.role rolllle\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "WHERE\n"
                + "	membre.id = " + id;
        List<Object[]> dataset = this.selectSQL(sql);
        if (dataset != null && dataset.size() > 0) {
            membre = new MembreAdapter();
            for (Object[] row : dataset) {
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try {
                    login.setRole((String) row[16]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

            }
        }

        return membre;
    }
    
    public MembreAdapter getMembre(String telephone) {
        MembreAdapter membre = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	login.username,\n"
                + "	login.password ,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	login.role\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	INNER JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	INNER JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "WHERE\n"
                + "	membre.telephone = '" + telephone + "'";
        List<Object[]> dataset = this.selectSQL(sql);
        if (dataset != null && dataset.size() > 0) {
            membre = new MembreAdapter();
            for (Object[] row : dataset) {
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setRole((String) row[16]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

            }
        }

        return membre;
    }

    public MembreAdapter getParrain(Integer fkMembre, String fkArbre) {
        MembreAdapter parrain = null;
        Parrainage parrainage = null;

        String hql = "from Parrainage where fkMembre =:membre and fkArbre =:arbre";

        List<Parrainage> list = this.selectHQL(hql, new QueryParam("membre", fkMembre), new QueryParam("arbre", fkArbre));

        if (list != null && list.size() > 0) {
            parrainage = list.get(0);
        }
        

        if (parrainage != null && parrainage.getFkParrain() != null) {
            parrain = this.getMembre(parrainage.getFkParrain());
        }
        

        return parrain;
    }

    public Administrateur getAdmin(String code) {
        Administrateur admin = null;

        admin = (Administrateur) this.findByCode(Administrateur.class, code);

        return admin;
    }

    public List<Paiement> getPaiements(Integer fkMembre) {
        List<Paiement> paiements = null;

        String sql
                = "SELECT\n"
                + "paiement.id,\n"
                + "paiement.fkMembre,\n"
                + "paiement.fkAdmin,\n"
                + "paiement.montantCDF,\n"
                + "paiement.montantUSD,\n"
                + "paiement.dateDemande,\n"
                + "paiement.datePaiement,\n"
                + "paiement.statut,\n"
                + "paiement.dateCreat,\n"
                + "membre.id AS id_0,\n"
                + "membre.nom,\n"
                + "membre.postnom,\n"
                + "membre.prenom,\n"
                + "membre.sexe,\n"
                + "membre.email,\n"
                + "membre.telephone,\n"
                + "paiement.etat,\n"
                + "paiement.fkManager,\n"
                + "paiement.fraisUSD,\n"
                + "paiement.fraisCDF,\n"
                + "paiement.montantTotalUSD,\n"
                + "paiement.montantTotalCDF\n"
                + "FROM\n"
                + "paiement paiement\n"
                + "JOIN membre membre\n"
                + "ON paiement.fkMembre = membre.id\n"
                + "WHERE paiement.statut <> 2 and  paiement.fkMembre =:fk order by dateCreat desc";
        
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fk", fkMembre));
        
        if(dataset != null && dataset.size() > 0){
            for(Object[] row: dataset){
                Paiement paiement = new Paiement();
                
                try{
                    paiement.setId((Integer) row[0]);
                }catch(Exception e){}
                try{
                    paiement.setFkMembre((Integer) row[1]);
                }catch(Exception e){}
                try{
                    paiement.setFkAdmin((String) row[2]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[3];
                    paiement.setMontantCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[4];
                    paiement.setMontantUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    paiement.setDateDemande((Date) row[5]);
                }catch(Exception e){}
                try{
                    paiement.setDatePaiement((Date) row[6]);
                }catch(Exception e){}
                try{
                    paiement.setStatut((Boolean) row[7]);
                }catch(Exception e){}
                try{
                    paiement.setDateCreat((Date) row[7]);
                }catch(Exception e){}
                
                Membre membre = new Membre();
                try{
                    membre.setId((Integer) row[8]);
                }catch(Exception e){}
                try{
                    membre.setNom((String) row[9]);
                }catch(Exception e){}
                try{
                    membre.setPostnom((String) row[10]);
                }catch(Exception e){}
                try{
                    membre.setPrenom((String) row[11]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setEmail((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setTelephone((String) row[13]);
                }catch(Exception e){}
                try{
                    paiement.setEtat((String) row[16]);
                }catch(Exception e){}
                try{
                    paiement.setFkManager((Integer) row[17]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[18];
                    paiement.setFraisUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[19];
                    paiement.setFraisCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[20];
                    paiement.setMontantTotalUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[21];
                    paiement.setMontantTotalCDF(big.doubleValue());
                }catch(Exception e){}
                
                paiement.setMembre(membre);
                
                if(paiements == null) paiements = new ArrayList<>();
                
                paiements.add(paiement);
            }
        }

        return paiements;
    }
    
    public List<Paiement> getPaiements(String fkAdmin) {
        List<Paiement> paiements = null;

        String sql
                = "SELECT\n"
                + "paiement.id,\n"
                + "paiement.fkMembre,\n"
                + "paiement.fkAdmin,\n"
                + "paiement.montantCDF,\n"
                + "paiement.montantUSD,\n"
                + "paiement.dateDemande,\n"
                + "paiement.datePaiement,\n"
                + "paiement.statut,\n"
                + "paiement.dateCreat,\n"
                + "membre.id AS id_0,\n"
                + "membre.nom,\n"
                + "membre.postnom,\n"
                + "membre.prenom,\n"
                + "membre.sexe,\n"
                + "membre.email,\n"
                + "membre.telephone,\n"
                + "paiement.etat,\n"
                + "paiement.fkManager,\n"
                + "paiement.fraisUSD,\n"
                + "paiement.fraisCDF,\n"
                + "paiement.montantTotalUSD,\n"
                + "paiement.montantTotalCDF\n"
                + "FROM\n"
                + "paiement paiement\n"
                + "JOIN membre membre\n"
                + "ON paiement.fkMembre = membre.id\n"
                + "WHERE paiement.statut <> 2 and  paiement.fkAdmin =:fk order by dateCreat desc";
        
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fk", fkAdmin));
        
        if(dataset != null && dataset.size() > 0){
            for(Object[] row: dataset){
                Paiement paiement = new Paiement();
                
                try{
                    paiement.setId((Integer) row[0]);
                }catch(Exception e){}
                try{
                    paiement.setFkMembre((Integer) row[1]);
                }catch(Exception e){}
                try{
                    paiement.setFkAdmin((String) row[2]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[3];
                    paiement.setMontantCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[4];
                    paiement.setMontantUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    paiement.setDateDemande((Date) row[5]);
                }catch(Exception e){}
                try{
                    paiement.setDatePaiement((Date) row[6]);
                }catch(Exception e){}
                try{
                    paiement.setStatut((Boolean) row[7]);
                }catch(Exception e){}
                try{
                    paiement.setDateCreat((Date) row[7]);
                }catch(Exception e){}
                
                Membre membre = new Membre();
                try{
                    membre.setId((Integer) row[8]);
                }catch(Exception e){}
                try{
                    membre.setNom((String) row[9]);
                }catch(Exception e){}
                try{
                    membre.setPostnom((String) row[10]);
                }catch(Exception e){}
                try{
                    membre.setPrenom((String) row[11]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setEmail((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setTelephone((String) row[13]);
                }catch(Exception e){}
                try{
                    paiement.setEtat((String) row[16]);
                }catch(Exception e){}
                try{
                    paiement.setFkManager((Integer) row[17]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[18];
                    paiement.setFraisUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[19];
                    paiement.setFraisCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[20];
                    paiement.setMontantTotalUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[21];
                    paiement.setMontantTotalCDF(big.doubleValue());
                }catch(Exception e){}
                
                paiement.setMembre(membre);
                
                if(paiements == null) paiements = new ArrayList<>();
                
                paiements.add(paiement);
            }
        }

        return paiements;
    }
    
    public List<Paiement> getPaiementsForMembre(Date debut, Date fin, Integer fkMembre) {
        List<Paiement> paiements = null;

        String sql
                = "SELECT\n"
                + "paiement.id,\n"
                + "paiement.fkMembre,\n"
                + "paiement.fkAdmin,\n"
                + "paiement.montantCDF,\n"
                + "paiement.montantUSD,\n"
                + "paiement.dateDemande,\n"
                + "paiement.datePaiement,\n"
                + "paiement.statut,\n"
                + "paiement.dateCreat,\n"
                + "membre.id AS id_0,\n"
                + "membre.nom,\n"
                + "membre.postnom,\n"
                + "membre.prenom,\n"
                + "membre.sexe,\n"
                + "membre.email,\n"
                + "membre.telephone,\n"
                + "paiement.etat,\n"
                + "paiement.fkManager,\n"
                + "paiement.fraisUSD,\n"
                + "paiement.fraisCDF,\n"
                + "paiement.montantTotalUSD,\n"
                + "paiement.montantTotalCDF\n"
                + "FROM\n"
                + "paiement paiement\n"
                + "JOIN membre membre\n"
                + "ON paiement.fkMembre = membre.id\n"
                + "WHERE paiement.statut <> 2 and  paiement.fkMembre =:fk and  paiement.dateDemande between :debut and :fin  order by dateCreat desc";
        
     
        
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fk", fkMembre), new QueryParam("debut", debut), new QueryParam("fin", fin));
        
        if(dataset != null && dataset.size() > 0){
            for(Object[] row: dataset){
                Paiement paiement = new Paiement();
                
                try{
                    paiement.setId((Integer) row[0]);
                }catch(Exception e){}
                try{
                    paiement.setFkMembre((Integer) row[1]);
                }catch(Exception e){}
                try{
                    paiement.setFkAdmin((String) row[2]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[3];
                    paiement.setMontantCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[4];
                    paiement.setMontantUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    paiement.setDateDemande((Date) row[5]);
                }catch(Exception e){}
                try{
                    paiement.setDatePaiement((Date) row[6]);
                }catch(Exception e){}
                try{
                    paiement.setStatut((Boolean) row[7]);
                }catch(Exception e){}
                try{
                    paiement.setDateCreat((Date) row[7]);
                }catch(Exception e){}
                
                Membre membre = new Membre();
                try{
                    membre.setId((Integer) row[8]);
                }catch(Exception e){}
                try{
                    membre.setNom((String) row[9]);
                }catch(Exception e){}
                try{
                    membre.setPostnom((String) row[10]);
                }catch(Exception e){}
                try{
                    membre.setPrenom((String) row[11]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setEmail((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setTelephone((String) row[13]);
                }catch(Exception e){}
                try{
                    paiement.setEtat((String) row[16]);
                }catch(Exception e){}
                try{
                    paiement.setFkManager((Integer) row[17]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[18];
                    paiement.setFraisUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[19];
                    paiement.setFraisCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[20];
                    paiement.setMontantTotalUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[21];
                    paiement.setMontantTotalCDF(big.doubleValue());
                }catch(Exception e){}
                
                paiement.setMembre(membre);
                
                if(paiements == null) paiements = new ArrayList<>();
                
                paiements.add(paiement);
            }
        }

        return paiements;
    }
    
    public List<Paiement> getPaiementsForManager(Date debut, Date fin, Integer fkMembre) {
        List<Paiement> paiements = null;

        String sql
                = "SELECT\n"
                + "paiement.id,\n"
                + "paiement.fkMembre,\n"
                + "paiement.fkAdmin,\n"
                + "paiement.montantCDF,\n"
                + "paiement.montantUSD,\n"
                + "paiement.dateDemande,\n"
                + "paiement.datePaiement,\n"
                + "paiement.statut,\n"
                + "paiement.dateCreat,\n"
                + "membre.id AS id_0,\n"
                + "membre.nom,\n"
                + "membre.postnom,\n"
                + "membre.prenom,\n"
                + "membre.sexe,\n"
                + "membre.email,\n"
                + "membre.telephone,\n"
                + "paiement.etat,\n"
                + "paiement.fkManager,\n"
                + "paiement.fraisUSD,\n"
                + "paiement.fraisCDF,\n"
                + "paiement.montantTotalUSD,\n"
                + "paiement.montantTotalCDF\n"
                + "FROM\n"
                + "paiement paiement\n"
                + "JOIN membre membre\n"
                + "ON paiement.fkMembre = membre.id\n"
                + "WHERE paiement.fkManager =:fk and  ((paiement.dateDemande between :debut and :fin) or (paiement.datePaiement between :debut and :fin))  order by dateCreat desc";
        
     
        
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fk", fkMembre), new QueryParam("debut", debut), new QueryParam("fin", fin));
        
        if(dataset != null && dataset.size() > 0){
            for(Object[] row: dataset){
                Paiement paiement = new Paiement();
                
                try{
                    paiement.setId((Integer) row[0]);
                }catch(Exception e){}
                try{
                    paiement.setFkMembre((Integer) row[1]);
                }catch(Exception e){}
                try{
                    paiement.setFkAdmin((String) row[2]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[3];
                    paiement.setMontantCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[4];
                    paiement.setMontantUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    paiement.setDateDemande((Date) row[5]);
                }catch(Exception e){}
                try{
                    paiement.setDatePaiement((Date) row[6]);
                }catch(Exception e){}
                try{
                    paiement.setStatut((Boolean) row[7]);
                }catch(Exception e){}
                try{
                    paiement.setDateCreat((Date) row[7]);
                }catch(Exception e){}
                
                Membre membre = new Membre();
                try{
                    membre.setId((Integer) row[8]);
                }catch(Exception e){}
                try{
                    membre.setNom((String) row[9]);
                }catch(Exception e){}
                try{
                    membre.setPostnom((String) row[10]);
                }catch(Exception e){}
                try{
                    membre.setPrenom((String) row[11]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setEmail((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setTelephone((String) row[13]);
                }catch(Exception e){}
                try{
                    paiement.setEtat((String) row[16]);
                }catch(Exception e){}
                try{
                    paiement.setFkManager((Integer) row[17]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[18];
                    paiement.setFraisUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[19];
                    paiement.setFraisCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[20];
                    paiement.setMontantTotalUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[21];
                    paiement.setMontantTotalCDF(big.doubleValue());
                }catch(Exception e){}
                
                paiement.setMembre(membre);
                
                if(paiements == null) paiements = new ArrayList<>();
                
                paiements.add(paiement);
            }
        }

        return paiements;
    }
    
    public List<Paiement> getPaiementsForAdmin(Date debut, Date fin, String fkMembre) {
        List<Paiement> paiements = null;

        String sql
                = "SELECT\n"
                + "paiement.id,\n"
                + "paiement.fkMembre,\n"
                + "paiement.fkAdmin,\n"
                + "paiement.montantCDF,\n"
                + "paiement.montantUSD,\n"
                + "paiement.dateDemande,\n"
                + "paiement.datePaiement,\n"
                + "paiement.statut,\n"
                + "paiement.dateCreat,\n"
                + "membre.id AS id_0,\n"
                + "membre.nom,\n"
                + "membre.postnom,\n"
                + "membre.prenom,\n"
                + "membre.sexe,\n"
                + "membre.email,\n"
                + "membre.telephone,\n"
                + "paiement.etat,\n"
                + "paiement.fkManager,\n"
                + "paiement.fraisUSD,\n"
                + "paiement.fraisCDF,\n"
                + "paiement.montantTotalUSD,\n"
                + "paiement.montantTotalCDF\n"
                + "FROM\n"
                + "paiement paiement\n"
                + "JOIN membre membre\n"
                + "ON paiement.fkMembre = membre.id\n"
                + "WHERE paiement.statut <> 2 and paiement.fkAdmin =:fk and  paiement.dateDemande between :debut and :fin  order by datePaiement desc";
        
     
        
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fk", fkMembre), new QueryParam("debut", debut), new QueryParam("fin", fin));
        
        if(dataset != null && dataset.size() > 0){
            for(Object[] row: dataset){
                Paiement paiement = new Paiement();
                
                try{
                    paiement.setId((Integer) row[0]);
                }catch(Exception e){}
                try{
                    paiement.setFkMembre((Integer) row[1]);
                }catch(Exception e){}
                try{
                    paiement.setFkAdmin((String) row[2]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[3];
                    paiement.setMontantCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[4];
                    paiement.setMontantUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    paiement.setDateDemande((Date) row[5]);
                }catch(Exception e){}
                try{
                    paiement.setDatePaiement((Date) row[6]);
                }catch(Exception e){}
                try{
                    paiement.setStatut((Boolean) row[7]);
                }catch(Exception e){}
                try{
                    paiement.setDateCreat((Date) row[7]);
                }catch(Exception e){}
                
                Membre membre = new Membre();
                try{
                    membre.setId((Integer) row[8]);
                }catch(Exception e){}
                try{
                    membre.setNom((String) row[9]);
                }catch(Exception e){}
                try{
                    membre.setPostnom((String) row[10]);
                }catch(Exception e){}
                try{
                    membre.setPrenom((String) row[11]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setEmail((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setTelephone((String) row[13]);
                }catch(Exception e){}
                try{
                    paiement.setEtat((String) row[16]);
                }catch(Exception e){}
                try{
                    paiement.setFkManager((Integer) row[17]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[18];
                    paiement.setFraisUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[19];
                    paiement.setFraisCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[20];
                    paiement.setMontantTotalUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[21];
                    paiement.setMontantTotalCDF(big.doubleValue());
                }catch(Exception e){}
                
                paiement.setMembre(membre);
                
                if(paiements == null) paiements = new ArrayList<>();
                
                paiements.add(paiement);
            }
        }

        return paiements;
    }
    
    public List<Paiement> getRaportPaiementsForAdmin(Date debut, Date fin, String fkMembre) {
        List<Paiement> paiements = null;

        String sql
                = "SELECT\n"
                + "paiement.id,\n"
                + "paiement.fkMembre,\n"
                + "paiement.fkAdmin,\n"
                + "paiement.montantCDF,\n"
                + "paiement.montantUSD,\n"
                + "paiement.dateDemande,\n"
                + "paiement.datePaiement,\n"
                + "paiement.statut,\n"
                + "paiement.dateCreat,\n"
                + "membre.id AS id_0,\n"
                + "membre.nom,\n"
                + "membre.postnom,\n"
                + "membre.prenom,\n"
                + "membre.sexe,\n"
                + "membre.email,\n"
                + "membre.telephone,\n"
                + "paiement.etat,\n"
                + "paiement.fkManager,\n"
                + "paiement.fraisUSD,\n"
                + "paiement.fraisCDF,\n"
                + "paiement.montantTotalUSD,\n"
                + "paiement.montantTotalCDF\n"
                + "FROM\n"
                + "paiement paiement\n"
                + "JOIN membre membre\n"
                + "ON paiement.fkMembre = membre.id\n"
                + "WHERE paiement.statut = 1 and paiement.fkAdmin =:fk and  paiement.dateDemande between :debut and :fin   order by datePaiement desc";
        
     
        
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("fk", fkMembre), new QueryParam("debut", debut), new QueryParam("fin", fin));
        
        if(dataset != null && dataset.size() > 0){
            for(Object[] row: dataset){
                Paiement paiement = new Paiement();
                
                try{
                    paiement.setId((Integer) row[0]);
                }catch(Exception e){}
                try{
                    paiement.setFkMembre((Integer) row[1]);
                }catch(Exception e){}
                try{
                    paiement.setFkAdmin((String) row[2]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[3];
                    paiement.setMontantCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[4];
                    paiement.setMontantUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    paiement.setDateDemande((Date) row[5]);
                }catch(Exception e){}
                try{
                    paiement.setDatePaiement((Date) row[6]);
                }catch(Exception e){}
                try{
                    paiement.setStatut((Boolean) row[7]);
                }catch(Exception e){}
                try{
                    paiement.setDateCreat((Date) row[7]);
                }catch(Exception e){}
                
                Membre membre = new Membre();
                try{
                    membre.setId((Integer) row[8]);
                }catch(Exception e){}
                try{
                    membre.setNom((String) row[9]);
                }catch(Exception e){}
                try{
                    membre.setPostnom((String) row[10]);
                }catch(Exception e){}
                try{
                    membre.setPrenom((String) row[11]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setEmail((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setTelephone((String) row[13]);
                }catch(Exception e){}
                try{
                    paiement.setEtat((String) row[16]);
                }catch(Exception e){}
                try{
                    paiement.setFkManager((Integer) row[17]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[18];
                    paiement.setFraisUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[19];
                    paiement.setFraisCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[20];
                    paiement.setMontantTotalUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[21];
                    paiement.setMontantTotalCDF(big.doubleValue());
                }catch(Exception e){}
                
                paiement.setMembre(membre);
                
                if(paiements == null) paiements = new ArrayList<>();
                
                paiements.add(paiement);
            }
        }

        return paiements;
    }
    
    public List<Paiement> getRaportPaiementsForPeriode(Date debut, Date fin) {
        List<Paiement> paiements = null;

        String sql
                = "SELECT\n"
                + "paiement.id,\n"
                + "paiement.fkMembre,\n"
                + "paiement.fkAdmin,\n"
                + "paiement.montantCDF,\n"
                + "paiement.montantUSD,\n"
                + "paiement.dateDemande,\n"
                + "paiement.datePaiement,\n"
                + "paiement.statut,\n"
                + "paiement.dateCreat,\n"
                + "membre.id AS id_0,\n"
                + "membre.nom,\n"
                + "membre.postnom,\n"
                + "membre.prenom,\n"
                + "membre.sexe,\n"
                + "membre.email,\n"
                + "membre.telephone,\n"
                + "paiement.etat,\n"
                + "paiement.fkManager,\n"
                + "paiement.fraisUSD,\n"
                + "paiement.fraisCDF,\n"
                + "paiement.montantTotalUSD,\n"
                + "paiement.montantTotalCDF\n"
                + "FROM\n"
                + "paiement paiement\n"
                + "JOIN membre membre\n"
                + "ON paiement.fkMembre = membre.id\n"
                + "WHERE paiement.statut = 1 and  paiement.datePaiement between :debut and :fin   order by datePaiement desc";
        
     
        
        List<Object[]> dataset = this.selectSQL(sql, new QueryParam("debut", debut), new QueryParam("fin", fin));
        
        if(dataset != null && dataset.size() > 0){
            for(Object[] row: dataset){
                Paiement paiement = new Paiement();
                
                try{
                    paiement.setId((Integer) row[0]);
                }catch(Exception e){}
                try{
                    paiement.setFkMembre((Integer) row[1]);
                }catch(Exception e){}
                try{
                    paiement.setFkAdmin((String) row[2]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[3];
                    paiement.setMontantCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[4];
                    paiement.setMontantUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    paiement.setDateDemande((Date) row[5]);
                }catch(Exception e){}
                try{
                    paiement.setDatePaiement((Date) row[6]);
                }catch(Exception e){}
                try{
                    paiement.setStatut((Boolean) row[7]);
                }catch(Exception e){}
                try{
                    paiement.setDateCreat((Date) row[7]);
                }catch(Exception e){}
                
                Membre membre = new Membre();
                try{
                    membre.setId((Integer) row[8]);
                }catch(Exception e){}
                try{
                    membre.setNom((String) row[9]);
                }catch(Exception e){}
                try{
                    membre.setPostnom((String) row[10]);
                }catch(Exception e){}
                try{
                    membre.setPrenom((String) row[11]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setEmail((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setSexe((String) row[12]);
                }catch(Exception e){}
                try{
                    membre.setTelephone((String) row[13]);
                }catch(Exception e){}
                try{
                    paiement.setEtat((String) row[16]);
                }catch(Exception e){}
                try{
                    paiement.setFkManager((Integer) row[17]);
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[18];
                    paiement.setFraisUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[19];
                    paiement.setFraisCDF(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[20];
                    paiement.setMontantTotalUSD(big.doubleValue());
                }catch(Exception e){}
                try{
                    BigDecimal big = (BigDecimal) row[21];
                    paiement.setMontantTotalCDF(big.doubleValue());
                }catch(Exception e){}
                
                paiement.setMembre(membre);
                
                if(paiements == null) paiements = new ArrayList<>();
                
                paiements.add(paiement);
            }
        }

        return paiements;
    }
    
    public boolean annulerPaiement(Integer id){
        boolean ok = false;
        
        String sql = "update paiement set etat = 'ANNULE', statut = 2 where id = " + id;
        
        ok = this.executeSQLQuery(sql);
        
        return ok;
    }
    
    public List<JournalAppro> getJournalApproForDestinateur(Integer fkDestinateur){
        List<JournalAppro> liste = null;
        
        String hql = "from JournalAppro where fkDestinateur =:fk";
        
        List<JournalAppro> dataset = this.selectHQL(hql, new QueryParam("fk", fkDestinateur));
        
        if(dataset != null && dataset.size() > 0){
            for(JournalAppro journal: dataset){
                journal.setDestinateur(this.getMembre(journal.getFkDestinateur()));
                journal.setProvenance(this.getMembre(journal.getFkProvenance()));
                
                if(liste == null)
                    liste = new ArrayList<>();
                liste.add(journal);
            }
        }
        
        return liste;
    }
    
    public List<JournalAppro> getJournalApproForProvenanceMembre(Integer fkDestinateur, Date debut, Date fin){
        List<JournalAppro> liste = null;
        
        String hql = "from JournalAppro where fkProvenance =:fk and dateCreat between :debut and :fin";
        
        List<JournalAppro> dataset = this.selectHQL(hql, new QueryParam("fk", fkDestinateur),  new QueryParam("debut", debut),new QueryParam("fin", fin));
        
        if(dataset != null && dataset.size() > 0){
            for(JournalAppro journal: dataset){
                journal.setDestinateur(this.getMembre(journal.getFkDestinateur()));
                journal.setProvenance(this.getMembre(journal.getFkProvenance()));
                
                try{
                    journal.setAgent((Administrateur) this.findById(Administrateur.class, journal.getFkAgent()));
                }catch(Exception e){}
                
                if(liste == null)
                    liste = new ArrayList<>();
                liste.add(journal);
            }
        }
        
        return liste;
    }
    
    public List<JournalAppro> getJournalApproForProvenanceAgent(Integer fkDestinateur, Date debut, Date fin){
        List<JournalAppro> liste = null;
        
        String hql = "from JournalAppro where fkAgent =:fk and dateCreat between :debut and :fin";
        
        List<JournalAppro> dataset = this.selectHQL(hql, new QueryParam("fk", fkDestinateur),  new QueryParam("debut", debut),new QueryParam("fin", fin));
        
        if(dataset != null && dataset.size() > 0){
            for(JournalAppro journal: dataset){
                journal.setDestinateur(this.getMembre(journal.getFkDestinateur()));
                journal.setProvenance(this.getMembre(journal.getFkProvenance()));
                
                try{
                    journal.setAgent((Administrateur) this.findById(Administrateur.class, journal.getFkAgent()));
                }catch(Exception e){}
                try{
                    journal.setAgentDestinateur((Administrateur) this.findById(Administrateur.class, journal.getFkAgentDestinateur()));
                }catch(Exception e){}
                
                if(liste == null)
                    liste = new ArrayList<>();
                liste.add(journal);
            }
        }
        
        return liste;
    }
    
    public List<JournalAppro> getJournalApproForDestinateur(Integer fkDestinateur, Date debut, Date fin){
        List<JournalAppro> liste = null;
        
        String hql = "from JournalAppro where fkDestinateur =:fk and dateCreat between :debut and :fin";
        
        List<JournalAppro> dataset = this.selectHQL(hql, new QueryParam("fk", fkDestinateur),  new QueryParam("debut", debut),new QueryParam("fin", fin));
        
        if(dataset != null && dataset.size() > 0){
            for(JournalAppro journal: dataset){
                journal.setDestinateur(this.getMembre(journal.getFkDestinateur()));
                journal.setProvenance(this.getMembre(journal.getFkProvenance()));
                
                try{
                    journal.setAgent((Administrateur) this.findById(Administrateur.class, journal.getFkAgent()));
                }catch(Exception e){}
                try{
                    journal.setAgentDestinateur((Administrateur) this.findById(Administrateur.class, journal.getFkAgentDestinateur()));
                }catch(Exception e){}
                
                if(liste == null)
                    liste = new ArrayList<>();
                liste.add(journal);
            }
        }
        
        return liste;
    }
    
    public List<JournalAppro> getJournalAppro(Date debut, Date fin, String type){
        List<JournalAppro> liste = null;
        
        String hql = "from JournalAppro where dateCreat between :debut and :fin";
        
        List<QueryParam> params = new ArrayList<>();
        params.add(new QueryParam("debut", debut));
        params.add(new QueryParam("fin", fin));
        
        if(type != null){
            hql += " and type =:type ";
            params.add(new QueryParam("type", type));
        }
        
        
        List<JournalAppro> dataset = this.selectHQL(hql, params);
        
        if(dataset != null && dataset.size() > 0){
            for(JournalAppro journal: dataset){
                journal.setDestinateur(this.getMembre(journal.getFkDestinateur()));
                journal.setProvenance(this.getMembre(journal.getFkProvenance()));
                
                try{
                    journal.setAgent((Administrateur) this.findById(Administrateur.class, journal.getFkAgent()));
                }catch(Exception e){}
                try{
                    journal.setAgentDestinateur((Administrateur) this.findById(Administrateur.class, journal.getFkAgentDestinateur()));
                }catch(Exception e){}
                
                if(liste == null)
                    liste = new ArrayList<>();
                liste.add(journal);
            }
        }
        
        return liste;
    }

}
