/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import entities.Administrateur;
import entities.Arbre;
import entities.Compte;
import entities.Login;
import entities.Membre;
import entities.QueryParam;
import entities.utilities.ArbreAdapter;
import entities.utilities.MembreAdapter;
import java.math.BigDecimal;
import java.util.List;
import others.Logger;

/**
 *
 * @author Trinix
 */
public class DaoAuthentification extends DaoGeneric {

    public MembreAdapter authentifier(String username, String password) {
        MembreAdapter membre = null;

        String sql
                = "SELECT\n"
                + "	membre.id id0, \n"
                + "	membre.nom, \n"
                + "	membre.postnom, \n"
                + "	membre.prenom,\n"
                + "	membre.sexe, \n"
                + "	membre.email, \n"
                + "	membre.telephone, \n"
                + "	membre.fkParrain, \n"
                + "	membre.fkArbre,\n"
                + "	login.id id3,\n"
                + "	login.username,\n"
                + "	login.`password`,\n"
                + "	login.token, \n"
                + "	compte.id id1, \n"
                + "	compte.soldeUSD, \n"
                + "	compte.soldeCDF,\n"
                + "	login.role, \n"
                + "	compte.approUSD, \n"
                + "	compte.approCDF\n"
                + "FROM\n"
                + "	membre AS membre\n"
                + "	LEFT JOIN\n"
                + "	login AS login\n"
                + "	ON \n"
                + "		membre.id = login.fkMembre\n"
                + "	LEFT JOIN\n"
                + "	compte AS compte\n"
                + "	ON \n"
                + "		membre.id = compte.fkMembre\n"
                + "WHERE\n"
                + "	login.username like :username AND\n"
                + "	login.`password` like :password AND\n"
                + "	membre.statut >= 0";

        List<Object[]> dataset = this.selectSQL(sql,/* new QueryParam("telephone", username),*/ new QueryParam("username", username), new QueryParam("password", password));
        if (dataset != null && dataset.size() > 0) {
            for (Object[] row : dataset) {
                membre = new MembreAdapter();
                try {
                    membre.setId((Integer) row[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setNom((String) row[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPostnom((String) row[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setPrenom((String) row[3]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setSexe((String) row[4]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setEmail((String) row[5]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setTelephone((String) row[6]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkParrain((Integer) row[7]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    membre.setFkArbre((String) row[8]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Login login = new Login();

                try {
                    login.setId((Integer) row[9]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setUsername((String) row[10]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setPassword((String) row[11]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    login.setToken((String) row[12]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Compte compte = new Compte();
                try {
                    compte.setId((Integer) row[13]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setSoldeUSD(((BigDecimal) row[14]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    BigDecimal bd = (BigDecimal) row[15];
                    compte.setSoldeCDF(bd.doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                
                try{
                    login.setRole((String) row[16]);
                }catch(Exception e){}
                
                try {
                    compte.setApproUSD(((BigDecimal) row[17]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    compte.setApproCDF(((BigDecimal) row[18]).doubleValue());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                membre.setLogin(login);
                membre.setCompte(compte);

                DaoMembre dao = new DaoMembre();

                List<ArbreAdapter> arbres = dao.getArbresAdapter(membre.getId());
                membre.setArbres(arbres);

            }
        }

        return membre;
    }
    
    public Administrateur connexion(String username, String password){
        Administrateur admin = null;
        
        String hql = "from Administrateur where username =:username and password=:password and  statut = " + true;
        
        List<Administrateur> list = this.selectHQL(hql, 1, new QueryParam("username", username), new QueryParam("password", password));
        Logger.printLog("CON", list.size() + "");
        if(list != null && list.size() > 0)
            admin = list.get(0);
        
        return admin;
    }
}
