/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import daos.DaoConfiguration;
import daos.DaoMembre;
import entities.Devise;
import entities.ErrorResponse;
import entities.ValueDataException;
import entities.utilities.ArbreAdapter;
import java.util.List;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
public class ConfigurationController {

    private ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }

    public ConfigurationController() {
        this.error = new ErrorResponse();
    }

    public List<Devise> getListDevises() {
        List<Devise> devises = null;

        try {
            DaoConfiguration dao = new DaoConfiguration();

            devises = dao.getList(Devise.class);
            
            if(devises == null || devises.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
            this.error.setErrorCode(ErrorResponse.OK);

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return devises;
    }
    
    public List<ArbreAdapter> getListArbre(){
        List<ArbreAdapter> arbres = null;
        
        try {
            DaoMembre dao = new DaoMembre();

            arbres = dao.getArbres();
            
            if(arbres == null || arbres.size() <= 0)
                throw new ValueDataException(AppConst.EMPTY_LIST);
            
            this.error.setErrorCode(ErrorResponse.OK);

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return arbres;
    }
}
