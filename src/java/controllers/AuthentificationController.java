/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import daos.DaoAuthentification;
import entities.Administrateur;
import entities.ErrorResponse;
import entities.ValueDataException;
import entities.utilities.MembreAdapter;
import others.AppUtilities;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
public class AuthentificationController {
    private ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }
    
    public AuthentificationController(){
        this.error = new ErrorResponse();
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }
    
    public MembreAdapter authentifier(String username, String password){
        MembreAdapter membre = null;
        
        try{
            AppUtilities.controlValue(username, "Veuillez renseigner le nom d'utilisateur");
            AppUtilities.controlValue(password, "Veuillez renseigner le mot de passe");
            
            DaoAuthentification dao = new DaoAuthentification();
            
            membre = dao.authentifier(username, password);
            
            if(membre == null)
                throw new ValueDataException("Nom d'utilisateur ou mot de passe incorrect");
            
            
            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Connexion avec succès");
            
        }catch(ValueDataException e){
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return membre;
    }
    
    public Administrateur connexion(String username, String password){
        Administrateur admin = null;
        
        try{
            AppUtilities.controlValue(username, "Veuillez renseigner le nom d'utilisateur");
            AppUtilities.controlValue(password, "Veuillez renseigner le mot de passe");
            
            DaoAuthentification dao = new DaoAuthentification();
            
            admin = dao.connexion(username, password);
            
            if(admin == null)
                throw new ValueDataException("Nom d''utilisateur ou mot de passe incorrect");
            
            this.error.setErrorCode(ErrorResponse.OK);
            
        }catch(ValueDataException e){
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }
        
        return admin;
    }
    
}
