/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import contracts.IGestionMembre;
import daos.DaoMembre;
import entities.Administrateur;
import entities.Arbre;
import entities.Compte;
import entities.Config;
import entities.ErrorResponse;
import entities.JournalAppro;
import entities.Login;
import entities.Membre;
import entities.Paiement;
import entities.Parrainage;
import entities.Partage;
import entities.ValueDataException;
import entities.utilities.ArbreAdapter;
import entities.utilities.MembreAdapter;
import entities.utilities.ParrainageAdapter;
import entities.utilities.RapportAdapter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;
import others.AppUtilities;
import others.Logger;
import utilities.AppConst;

/**
 *
 * @author Trinix
 */
public class MembreController implements IGestionMembre {

    private ErrorResponse error;

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }

    public MembreController() {
        this.error = new ErrorResponse();
    }

    public boolean updateLogin(Login login) {
        boolean done = false;

        try {

            DaoMembre dao = new DaoMembre();

            login.setStatut(true);
            login.setIsFirstConnection(true);

            done = dao.saveOrUpdate(login);

            if (!done) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return false;
    }

    @Override
    public Membre parrainer(MembreAdapter membre) {
        Membre m = null;

        try {
            AppUtilities.controlValue(membre.getNom(), "Veuillez entrer le nom");
            AppUtilities.controlValue(membre.getTelephone(), "Veuillez renseigner le parrain");
            AppUtilities.controlValue(membre.getFkArbre(), "Veuillez renseigner le branche");

            DaoMembre dao = new DaoMembre();

            if (!dao.isActivatedInBranche(membre.getFkParrain(), membre.getFkArbre())) {
                throw new ValueDataException("Vous n'avez pas été activé dans cette branche. Veuillez vous faire activer pour commencer le parrainage");
            }
            if (!dao.isValid(membre.getFkParrain(), membre.getFkArbre())) {
                throw new ValueDataException("Vous avez atteint la limite des filleuls pour cette branche");
            }

            if (dao.existInBranche(membre.getTelephone(), membre.getFkArbre())) {
                throw new ValueDataException("Ce membre existe déjà dans cette branche");
            }

            m = membre.convert();
            m.setStatut(true);
            m.setDateCreat(new Date());

            Integer id = dao.saveMembre(m);

            if (id == null) {
                throw new ValueDataException("Une erreur s'est produite lors de l'enregistrement");
            }

            m.setId(id);

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Filleul enregistré avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return m;
    }

    public Membre inscription(MembreAdapter membre) {
        Membre m = null;

        try {
            AppUtilities.controlValue(membre.getNom(), "Veuillez entrer le nom");
            AppUtilities.controlValue(membre.getTelephone(), "Veuillez renseigner le parrain");
            AppUtilities.controlValue(membre.getFkArbre(), "Veuillez renseigner le branche");

            DaoMembre dao = new DaoMembre();

            if (!dao.isActivatedInBranche(membre.getFkParrain(), membre.getFkArbre())) {
                throw new ValueDataException("Vous n'avez pas été activé dans cette branche. Veuillez vous faire activer pour commencer le parrainage");
            }
            if (!dao.isValid(membre.getFkParrain(), membre.getFkArbre())) {
                throw new ValueDataException("Vous avez atteint la limite des filleuls pour cette branche");
            }
            if (dao.existInBranche(membre.getTelephone(), membre.getFkArbre())) {
                throw new ValueDataException("Ce membre existe déjà dans cette branche");
            }

            m = membre.convert();
            m.setStatut(true);
            m.setDateCreat(new Date());

            Integer id = dao.saveMembre(m, membre.getLogin());

            if (id == null) {
                throw new ValueDataException("Une erreur s'est produite lors de l'enregistrement");
            }

            m.setId(id);

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Votre compte a été crée avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return m;
    }

    @Override
    public ArbreAdapter creerArbre(Arbre arbre) {
        ArbreAdapter adapter = null;

        try {
            // AppUtilities.controlValue(arbre, AppConst.PARAMS_INCORRECT_FORMAT);
            AppUtilities.controlValue(arbre.getDescription(), "Veuillez entrer une description");
            AppUtilities.controlValue(arbre.getFkGeniteur(), "Veuillez renseigner le geniteur de la branche");
            AppUtilities.controlValue(arbre.getFkDevise(), "Veuillew renseigner la devise");
            AppUtilities.controlValue(arbre.getFkPartage(), "Veuillez renseigner le modele de partage");
            AppUtilities.controlValue(arbre.getMontant(), "Veuiilez renseigner le montant d'adhesion");

            DaoMembre dao = new DaoMembre();

            if (!dao.isAllArbreCorrect2(arbre.getFkGeniteur())) {
                throw new ValueDataException("Veuillez au moins deux générations dans chacune vos branches actuels");
            }

            adapter = dao.createArbre(arbre.getFkGeniteur(), arbre.getFkPartage(), arbre.getMontant(), arbre.getDescription(), arbre.getFkDevise());

            if (adapter == null) {
                throw new ValueDataException("La branche n'a pas été créée");
            }

            Parrainage parrainage = new Parrainage();
            parrainage.setFkMembre(adapter.getFkGeniteur());
            parrainage.setFkArbre(adapter.getCode());
            parrainage.setStatut(false);
            parrainage.setDateCreat(new Date());
            parrainage.setIsAgent(true);
            
            Integer id = dao.save(parrainage);
            if(id == null)
                throw new ValueDataException(AppConst.ERROR_UNKNOW);

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Branche crée avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_SERVLET_UNKNOW);
        }

        return adapter;
    }

    public boolean activerMembre(String telephone) {
        boolean activated = false;

        try {
            AppUtilities.controlValue(telephone, "Veuillez renseigner le membre");

            DaoMembre dao = new DaoMembre();

            activated = dao.activerMembre(telephone);

            this.error.setErrorCode(ErrorResponse.OK);

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return activated;
    }

    public boolean activerMembre(Integer id, Integer fkAgent) {
        boolean activated = false;

        try {
            AppUtilities.controlValue(id, "Veuillez renseigner le parrainage");
            AppUtilities.controlValue(fkAgent, "Veuillez renseigner l'agent");

            DaoMembre dao = new DaoMembre();

            Administrateur admin = (Administrateur) dao.findById(Administrateur.class, fkAgent);

            if (admin == null) {
                throw new ValueDataException("L'agent n'exsite pas dans le système");
            }
            Parrainage parrainage = (Parrainage) dao.findById(Parrainage.class, id);

            if (parrainage == null) {
                throw new ValueDataException("Ce parrainage n'existe pas dans le système");
            }

            if (parrainage.getDateActivation() != null) {
                throw new ValueDataException("Ce membre a déjà été activé pour cette branche");
            }

            parrainage.setStatut(true);
            parrainage.setFkAgent(fkAgent);
            parrainage.setDateActivation(new Date());
            parrainage.setIsAgent(true);

            if (!dao.saveOrUpdate(parrainage)) {
                throw new ValueDataException("L'activation a échoué. Veuillez ressayer ou contacter le support technique");
            }

            activated = true;

            if (isIntegration(parrainage)) {
                partagerGain(parrainage);
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("L'activation effectué avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return activated;
    }

    public boolean activerMembreManager(Integer id, Integer fkAgent) {
        boolean activated = false;

        try {
            AppUtilities.controlValue(id, "Veuillez renseigner le parrainage");
            AppUtilities.controlValue(fkAgent, "Veuillez renseigner l'agent");

            DaoMembre dao = new DaoMembre();

            MembreAdapter membre = dao.getMembre(fkAgent);

            if (membre == null) {
                throw new ValueDataException("Ce membre n'exsite pas dans le système");
            }
            if (!membre.getLogin().getRole().equals("manager")) {
                throw new ValueDataException("Ce membre n'a pas le droit d'effectuer cette action");
            }

            Compte compte = dao.getCompteMembre(membre.getId());

            if (compte == null) {
                throw new ValueDataException("Ce membre n'a pas de compte");
            }

//            activated = dao.activatedMembre(id, fkAgent);
//            if(!activated)
//                throw new ValueDataException(AppConst.OPERATION_FAILED);
            Parrainage parrainage = (Parrainage) dao.findById(Parrainage.class, id);

            if (parrainage == null) {
                throw new ValueDataException("Ce parrainage n'existe pas dans le système");
            }

            ArbreAdapter arbre = dao.getAbreAdapter(parrainage.getFkArbre());

            if (arbre.getDevise().getCode().toLowerCase().equals("cdf")) {
                if (compte.getApproCDF() < arbre.getMontant()) {
                    throw new ValueDataException("Votre solde CDF d'activation est insuffisant");
                }

                compte.setApproCDF(compte.getApproCDF() - arbre.getMontant());

                if (!dao.saveOrUpdate(compte)) {
                    throw new ValueDataException(AppConst.OPERATION_FAILED);
                }

            }
            if (arbre.getDevise().getCode().toLowerCase().equals("usd")) {
                if (compte.getApproUSD() < arbre.getMontant()) {
                    throw new ValueDataException("Votre solde USD d'activation est insuffisant");
                }

                compte.setApproUSD(compte.getApproUSD() - arbre.getMontant());

                if (!dao.saveOrUpdate(compte)) {
                    throw new ValueDataException(AppConst.OPERATION_FAILED);
                }

            }

            parrainage.setStatut(true);
            parrainage.setFkAgent(fkAgent);
            parrainage.setDateActivation(new Date());
            parrainage.setIsAgent(false);

            if (!dao.saveOrUpdate(parrainage)) {
                throw new ValueDataException("L'activation a échoué. Veuillez ressayer ou contacter le support technique");
            }

            activated = true;

            if (isIntegration(parrainage)) {
                Logger.printLog("CTRL", "integration");
                partagerGain(parrainage);
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("L'activation effectué avec suucès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return activated;
    }

    public boolean isIntegration(Parrainage parrainage) {
        boolean isIntegration = false;
        DaoMembre dao = new DaoMembre();

        isIntegration = dao.isIntegeration(parrainage.getFkParrain(), parrainage.getFkArbre());

        return isIntegration;
    }

    public void mouvementCompte(Integer fkMembre, String fkArbre, Integer generation) {
        DaoMembre dao = new DaoMembre();

        MembreAdapter membre = dao.getMembre(fkMembre);

        if (membre == null) {
            return;
        }

        Compte compte = (Compte) dao.findById(Compte.class, membre.getCompte().getId());

        if (compte == null) {
            return;
        }

        ArbreAdapter arbre = dao.getAbreAdapter(fkArbre);
        if (arbre == null) {
            return;
        }

        Partage partage = (Partage) dao.findById(Partage.class, arbre.getPartage().getId());

        switch (generation) {
            case 1:
                Logger.printLog("CTRL AVANT G1", fkMembre + " " + compte.getSoldeUSD());
                if (arbre.getDevise().getCode().toLowerCase().equals("usd")) {
                    compte.setSoldeUSD(compte.getSoldeUSD() + (arbre.getMontant() * partage.getG1() * 5));
                }
                if (arbre.getDevise().getCode().toLowerCase().equals("cdf")) {
                    compte.setSoldeCDF(compte.getSoldeCDF() + (arbre.getMontant() * partage.getG1() * 5));
                }

                Logger.printLog("CTRL APRES G1", fkMembre + " " + compte.getSoldeUSD());
                break;

            case 2:
                Logger.printLog("CTRL AVANT G2", fkMembre + " " + compte.getSoldeUSD());
                if (arbre.getDevise().getCode().toLowerCase().equals("usd")) {
                    compte.setSoldeUSD(compte.getSoldeUSD() + (arbre.getMontant() * partage.getG2() * 5));
                }
                if (arbre.getDevise().getCode().toLowerCase().equals("cdf")) {
                    compte.setSoldeCDF(compte.getSoldeCDF() + (arbre.getMontant() * partage.getG2() * 5));
                }

                Logger.printLog("CTRL APRES G2", fkMembre + " " + compte.getSoldeUSD());
                break;

            case 3:
                Logger.printLog("CTRL AVANT G3", fkMembre + " " + compte.getSoldeUSD());
                if (arbre.getDevise().getCode().toLowerCase().equals("usd")) {
                    compte.setSoldeUSD(compte.getSoldeUSD() + (arbre.getMontant() * partage.getG3() * 5));
                }
                if (arbre.getDevise().getCode().toLowerCase().equals("cdf")) {
                    compte.setSoldeCDF(compte.getSoldeCDF() + (arbre.getMontant() * partage.getG3() * 5));
                }

                Logger.printLog("CTRL APRES G3", fkMembre + " " + compte.getSoldeUSD());

                break;

        }

        dao.saveOrUpdate(compte);
    }

    public void partagerGain(Parrainage parrainage) {
        if (parrainage == null) {
            return;
        }

        DaoMembre dao = new DaoMembre();

        //Recuperation du parrain
        MembreAdapter m = dao.getParrain(parrainage.getFkMembre(), parrainage.getFkArbre());
        mouvementCompte(m.getId(), parrainage.getFkArbre(), 1);

        MembreAdapter m1 = dao.getParrain(m.getId(), parrainage.getFkArbre());

        if (m1 == null) {
            return;
        }
        mouvementCompte(m1.getId(), parrainage.getFkArbre(), 2);

        MembreAdapter m2 = dao.getParrain(m1.getId(), parrainage.getFkArbre());

        if (m2 == null) {
            return;
        }
        mouvementCompte(m2.getId(), parrainage.getFkArbre(), 3);

    }

    public Membre parrainerAutreMembre(Integer fkMembre, Integer fkParrain, String fkArbre) {
        Membre membre = null;

        try {
            AppUtilities.controlValue(fkMembre, "Veuillez renseinger le membre");
            //AppUtilities.controlValue(fkParrain, "Veuillez renseigner le parrain");
            AppUtilities.controlValue(fkArbre, "Vuillez renseigner l'arbre");

            Parrainage parrainage = new Parrainage();
            parrainage.setDateCreat(new Date());
            parrainage.setStatut(false);
            parrainage.setFkMembre(fkMembre);
            parrainage.setFkParrain(fkParrain);
            parrainage.setFkArbre(fkArbre);

            DaoMembre dao = new DaoMembre();

            Integer id = dao.save(parrainage);

            if (id == null) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            membre = (Membre) dao.findById(Membre.class, fkMembre);

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Operation éffectuée avec suucès");
        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return membre;
    }

    public boolean isAllBrancheCorrecte(Integer fkMembre) {
        boolean isCorrect = false;

        DaoMembre dao = new DaoMembre();

        return isCorrect;
    }

    public List<MembreAdapter> getListMembre(String motclef) {
        List<MembreAdapter> list = null;

        try {
            AppUtilities.controlValue(motclef, "Veuillez saisir le mot clef");

            DaoMembre dao = new DaoMembre();

            list = dao.getListMembre(motclef);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucun membre trouvé");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " Membre trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<ParrainageAdapter> getListParrainage(String motclef) {
        List<ParrainageAdapter> list = null;

        try {
            AppUtilities.controlValue(motclef, "Veuillez saisir le mot clef");

            DaoMembre dao = new DaoMembre();

            list = dao.getListParrainages(motclef);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucun membre trouvé");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " Membres trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<ParrainageAdapter> getListParrainageParDate(String debut, String fin) {
        List<ParrainageAdapter> list = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez saisir la date de debut");
            AppUtilities.controlValue(fin, "Veuillez saisir la date de fin");

            DaoMembre dao = new DaoMembre();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date dateFin = null;

            try {
                dateFin = sdf.parse(fin);
                if(dateFin == null)
                    dateFin = sdf.parse(debut);
                dateFin = utilities.Utilities.transformEndDay(dateFin);
            } catch (Exception e) {
            }

            list = dao.getListParrainages(sdf.parse(debut), dateFin);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucun membre trouvé");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " Membre trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<ParrainageAdapter> getListParrainageParDateActivation(String debut, String fin) {
        List<ParrainageAdapter> list = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez saisir la date de debut");
            //AppUtilities.controlValue(fin, "Veuillez saisir la date de fin");

            DaoMembre dao = new DaoMembre();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date dateFin = null;
            try {
                dateFin = sdf.parse(fin);
                if (dateFin == null) {
                    dateFin = sdf.parse(debut);
                }

            } catch (Exception e) {
            }
            dateFin = utilities.Utilities.transformEndDay(dateFin);

            list = dao.getListParrainagesActivated(sdf.parse(debut), dateFin);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucun membre trouvé");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " rappainages trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<ParrainageAdapter> getListParrainageParDateActivationManager(String debut, String fin, Integer fkManager) {
        List<ParrainageAdapter> list = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez saisir la date de debut");
            AppUtilities.controlValue(fkManager, "Veuillez reigner le manager");

            DaoMembre dao = new DaoMembre();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date dateFin = null;
            try {
                dateFin = sdf.parse(fin);

            } catch (Exception e) {
            }
            try {
                if (dateFin == null) {
                    dateFin = sdf.parse(debut);
                }
            } catch (Exception e) {
            }
            dateFin = utilities.Utilities.transformEndDay(dateFin);

            list = dao.getListParrainagesActivatedForManager(sdf.parse(debut), dateFin, fkManager);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucun membre trouvé");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " rappainages trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<ParrainageAdapter> getRapportActivation(String debut, String fin, Integer fkAgent) {
        List<ParrainageAdapter> list = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez saisir la date de debut");
            //AppUtilities.controlValue(fin, "Veuillez saisir la date de fin");
            AppUtilities.controlValue(fkAgent, "Veuillez resnseigner l'agent");

            if (fin == null) {
                fin = debut;
            }

            DaoMembre dao = new DaoMembre();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date dateFin = null;

            try {

                dateFin = sdf.parse(fin);

                dateFin = utilities.Utilities.transformEndDay(dateFin);

            } catch (Exception e) {
                e.printStackTrace();
            }

            list = dao.getListParrainages(sdf.parse(debut), dateFin, fkAgent);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucune opération éffectuée.");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " Membre trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<Paiement> getRapportPaiement(String debut, String fin, String fk) {
        List<Paiement> list = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez renseigner la date de debut");
            AppUtilities.controlValue(fk, "Veuillez renseigner l'agent");

            if (fin == null || fin.isEmpty()) {
                fin = debut;
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date dateDebut = sdf.parse(debut);
            Date dateFin = null;

            try {

                dateFin = sdf.parse(fin);

                dateFin = utilities.Utilities.transformEndDay(dateFin);

            } catch (Exception e) {
            }

            DaoMembre dao = new DaoMembre();

            list = dao.getRaportPaiementsForAdmin(dateDebut, dateFin, fk);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucune opération retrouvée.");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " paiements trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public RapportAdapter getRapport(String debut, String fin, Integer fkAgent) {
        RapportAdapter rapport = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez saisir la date de debut");
            //AppUtilities.controlValue(fin, "Veuillez saisir la date de fin");
            AppUtilities.controlValue(fkAgent, "Veuillez resnseigner l'agent");

            DaoMembre dao = new DaoMembre();

            Administrateur admin = (Administrateur) dao.findById(Administrateur.class, fkAgent);

            if (admin == null) {
                throw new ValueDataException("Cet agent n'existe pas dans le systéme");
            }

            rapport = new RapportAdapter();

            rapport.setPaiements(this.getRapportPaiement(debut, fin, admin.getCode()));
            rapport.setParrainages(this.getRapportActivation(debut, fin, fkAgent));

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return rapport;
    }

    public entities.Config getConfig(String code) {
        entities.Config config = null;

        try {
            AppUtilities.controlValue(code, "Veuiller renseigner le code");

            DaoMembre dao = new DaoMembre();

            config = (Config) dao.findByCode(Config.class, code);
            if (config == null) {
                throw new ValueDataException("Aucune configutation trouvée");
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return config;
    }

    public Integer effectuerRetrait(Paiement paiement) {
        Integer id = null;

        try {
            AppUtilities.controlValue(paiement.getFkMembre(), "Veuillez renseigner le membre");
            AppUtilities.controlValue(paiement.getFkAdmin(), "Veuillez renseigner l'agent");

            Config config = this.getConfig(AppConst.FRAIS_RETRAIT_CODE);

            if (config == null) {
                throw new ValueDataException(this.error.getErrorDescription());
            }

            paiement.setFraisUSD(paiement.getMontantUSD() * config.getValueDecimal() / 100);
            paiement.setFraisCDF(paiement.getMontantCDF() * config.getValueDecimal() / 100);

            paiement.setMontantTotalUSD(paiement.getMontantUSD() + paiement.getFraisUSD());
            paiement.setMontantTotalCDF(paiement.getMontantCDF() + paiement.getFraisCDF());

            paiement.setDateCreat(new Date());
            paiement.setDateDemande(new Date());
            paiement.setStatut(false);
            paiement.setEtat("ATTENTE");
            paiement.setIsAgent(true);

            DaoMembre dao = new DaoMembre();

            Administrateur admin = dao.getAdmin(paiement.getFkAdmin());

            if (admin == null) {
                throw new ValueDataException("Cet agent n'existe pas dans le système");
            }

            if (!isSoldeSuffissante(paiement)) {
                throw new ValueDataException(this.error.getErrorDescription());
            }

            id = dao.save(paiement);

            if (id == null) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Demande effectuée avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return id;
    }

    public Integer effectuerRetraitFromManager(Paiement paiement) {
        Integer id = null;

        try {
            AppUtilities.controlValue(paiement.getFkMembre(), "Veuillez renseigner le membre");
            AppUtilities.controlValue(paiement.getFkManager(), "Veuillez renseigner l'agent");

            Config config = this.getConfig(AppConst.FRAIS_RETRAIT_CODE);

            if (config == null) {
                throw new ValueDataException(this.error.getErrorDescription());
            }

            paiement.setFraisUSD(paiement.getMontantUSD() * config.getValueDecimal() / 100);
            paiement.setFraisCDF(paiement.getMontantCDF() * config.getValueDecimal() / 100);

            paiement.setMontantTotalUSD(paiement.getMontantUSD() + paiement.getFraisUSD());
            paiement.setMontantTotalCDF(paiement.getMontantCDF() + paiement.getFraisCDF());

            paiement.setDateCreat(new Date());
            paiement.setDateDemande(new Date());
            paiement.setStatut(false);
            paiement.setEtat("ATTENTE");
            paiement.setIsAgent(false);

            DaoMembre dao = new DaoMembre();

            MembreAdapter admin = dao.getMembre(paiement.getFkManager());

            if (admin == null) {
                throw new ValueDataException("Ce manager n'existe pas dans le système");
            }

            if (!admin.getLogin().getRole().toLowerCase().equals("manager")) {
                throw new ValueDataException("Ce membre n'a pas le droit d'éffectuer un paiement");
            }

            if (!isSoldeSuffissante(paiement)) {
                throw new ValueDataException(this.error.getErrorDescription());
            }

            id = dao.save(paiement);

            if (id == null) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Demande effectuée avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return id;
    }

    public Boolean effectuerPaiement(Integer fk) {
        boolean id = false;

        try {
            AppUtilities.controlValue(fk, "Veuillez renseigner la reference de ");

            DaoMembre dao = new DaoMembre();

            Paiement paiement = (Paiement) dao.findById(Paiement.class, fk);

            if (paiement == null) {
                throw new ValueDataException("Cette demande n'existe pas dans le système");
            }

            if (!isSoldeSuffissante(paiement)) {
                throw new ValueDataException(this.error.getErrorDescription());
            }

            if (!retirerMontant(paiement.getFkMembre(), paiement.getMontantTotalCDF(), paiement.getMontantTotalUSD())) {
                throw new ValueDataException(this.error.getErrorDescription());
            }

            paiement.setDatePaiement(new Date());
            paiement.setEtat("PAYE");
            paiement.setStatut(true);

            id = dao.saveOrUpdate(paiement);

            if (!id) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Paiement effectué avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return id;
    }

    public Boolean effectuerPaiementManager(Integer fk) {
        boolean id = false;

        try {
            AppUtilities.controlValue(fk, "Veuillez renseigner la reference de ");

            DaoMembre dao = new DaoMembre();

            Paiement paiement = (Paiement) dao.findById(Paiement.class, fk);

            if (paiement == null) {
                throw new ValueDataException("Cette demande n'existe pas dans le système");
            }

            if (!isSoldeSuffissante(paiement)) {
                throw new ValueDataException(this.error.getErrorDescription());
            }

            if (!retirerMontant(paiement.getFkMembre(), paiement.getMontantTotalCDF(), paiement.getMontantTotalUSD())) {
                throw new ValueDataException(this.error.getErrorDescription());
            }

            if (paiement.getIsAgent()) {
                throw new ValueDataException("Cette demande n'est pas destiné à un manager");
            }

            if (!this.accrediterCompteManager(paiement.getFkManager(), paiement.getMontantTotalCDF(), paiement.getMontantTotalUSD())) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            paiement.setDatePaiement(new Date());
            paiement.setEtat("PAYE");
            paiement.setStatut(true);

            id = dao.saveOrUpdate(paiement);

            if (!id) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Paiement effectué avec succès");

        } catch (ValueDataException e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return id;
    }

    private boolean isSoldeSuffissante(Paiement paiement) {
        boolean ok = false;

        try {

            DaoMembre dao = new DaoMembre();

            Compte compte = dao.getCompteMembre(paiement.getFkMembre());

            if (compte == null) {
                throw new ValueDataException("Ce membre n'a aps de compte");
            }

            if (compte.getSoldeCDF() < paiement.getMontantCDF()) {
                throw new ValueDataException("Solde CDF de compte est insuffisant");
            }
            if (compte.getSoldeUSD() < paiement.getMontantUSD()) {
                throw new ValueDataException("Solde USD de compte est insuffisant");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            ok = true;

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return ok;
    }

    private boolean retirerMontant(Integer fkMembre, Double montantCDF, Double montantUSD) {
        boolean ok = false;

        try {

            DaoMembre dao = new DaoMembre();

            Compte compte = dao.getCompteMembre(fkMembre);

            if (compte == null) {
                throw new ValueDataException("Ce membre n'a aps de compte");
            }

            compte.setSoldeCDF(compte.getSoldeCDF() - montantCDF);
            compte.setSoldeUSD(compte.getSoldeUSD() - montantUSD);

            if (!dao.saveOrUpdate(compte)) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            this.error.setErrorCode(ErrorResponse.OK);
            ok = true;

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return ok;
    }

    public List<Paiement> getPaiementForMembre(Integer fk) {
        List<Paiement> list = null;

        try {
            AppUtilities.controlValue(fk, "Veuillez renseigner le membre");

            DaoMembre dao = new DaoMembre();

            list = dao.getPaiements(fk);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucune opération retrouvée.");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " paiements trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<Paiement> getPaiementForAdmin(String fk) {
        List<Paiement> list = null;

        try {
            AppUtilities.controlValue(fk, "Veuillez renseigner l'agent");

            DaoMembre dao = new DaoMembre();

            list = dao.getPaiements(fk);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucune opération retrouvée.");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " paiements trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<Paiement> getPaiementForAdmin(String debut, String fin, String fk) {
        List<Paiement> list = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez renseigner la date de debut");
            AppUtilities.controlValue(fk, "Veuillez renseigner l'agent");

            if (fin == null || fin.isEmpty()) {
                fin = debut;
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date dateDebut = sdf.parse(debut);

            Date dateFin = null;

            try {
                dateFin = sdf.parse(fin);
                if(dateFin == null)
                    dateFin = sdf.parse(debut);
                dateFin = utilities.Utilities.transformEndDay(dateFin);
            } catch (Exception e) {
            }

            DaoMembre dao = new DaoMembre();

            list = dao.getPaiementsForAdmin(dateDebut, dateFin, fk);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucune opération retrouvée.");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " paiements trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<Paiement> getPaiementForPeriode(String debut, String fin) {
        List<Paiement> list = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez renseigner la date de debut");

            if (fin == null || fin.isEmpty()) {
                fin = debut;
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date dateDebut = sdf.parse(debut);
            Date dateFin = null;

            try {
                dateFin = sdf.parse(fin);
                if(dateFin == null)
                    dateFin = sdf.parse(debut);
                dateFin = utilities.Utilities.transformEndDay(dateFin);
            } catch (Exception e) {
            }

            DaoMembre dao = new DaoMembre();

            list = dao.getRaportPaiementsForPeriode(dateDebut, dateFin);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucune opération retrouvée.");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " paiements trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<Paiement> getPaiementForMembre(String debut, String fin, Integer fk) {
        List<Paiement> list = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez renseigner la date de debut");
            AppUtilities.controlValue(fk, "Veuillez renseigner le membre");

            if (fin == null || fin.isEmpty()) {
                fin = debut;
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date dateDebut = sdf.parse(debut);
            Date dateFin = null;

            try {
                dateFin = sdf.parse(fin);
                if(dateFin == null)
                    dateFin = sdf.parse(debut);
                dateFin = utilities.Utilities.transformEndDay(dateFin);
            } catch (Exception e) {
            }

            DaoMembre dao = new DaoMembre();

            list = dao.getPaiementsForMembre(dateDebut, dateFin, fk);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucune opération retrouvée.");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " paiements trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<Paiement> getPaiementForManager(String debut, String fin, Integer fk) {
        List<Paiement> list = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez renseigner la date de debut");
            AppUtilities.controlValue(fk, "Veuillez renseigner le membre");

            if (fin == null || fin.isEmpty()) {
                fin = debut;
            }

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date dateDebut = sdf.parse(debut);
            Date dateFin = null;

            try {
                dateFin = sdf.parse(fin);
                if(dateFin == null)
                    dateFin = sdf.parse(debut);
                dateFin = utilities.Utilities.transformEndDay(dateFin);
            } catch (Exception e) {
            }

            DaoMembre dao = new DaoMembre();

            list = dao.getPaiementsForManager(dateDebut, dateFin, fk);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucune opération retrouvée.");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " paiements trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public boolean annulerPaiement(Integer id) {
        boolean ok = false;

        try {
            AppUtilities.controlValue(id, "Veuillez renseigner la demande de paiment");

            DaoMembre dao = new DaoMembre();

            Paiement paiement = (Paiement) dao.findById(Paiement.class, id);

            if (paiement == null) {
                throw new ValueDataException("Cette demande n'existe pas dans le système");
            }

            ok = dao.annulerPaiement(id);

            if (!ok) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription("Paiement anuulé avec succès");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return ok;
    }

    public MembreAdapter getMembre(String telephone) {
        MembreAdapter membre = null;

        try {
            AppUtilities.controlValue(telephone, "Veuillez renseigner le telephone");

            DaoMembre dao = new DaoMembre();

            membre = dao.getMembre(telephone);

            if (membre == null) {
                throw new ValueDataException("Aucun compte retrouvé pour ce numéro");
            }

            this.error.setErrorCode("OK");
        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return membre;
    }

    public boolean approvisionnementAgentToManager(Integer fkDestinateur, Integer fkProvenance, Double montantcdf, Double montantusd) {
        boolean done = false;
        String type = "manager";

        try {
            AppUtilities.controlValue(fkDestinateur, "veuillez renseigner le destinateur");
            AppUtilities.controlValue(fkProvenance, "veuillez renseigner l'agent MLI");
            //AppUtilities.controlValue(montantusd, "Veuillez renseigner le montant usd");
            //AppUtilities.controlValue(montantcdf, "Veuillez renseigner le montant cdf");

            DaoMembre dao = new DaoMembre();

            MembreAdapter membre = dao.getMembre(fkDestinateur);

            Config config = (Config) dao.findByCode(Config.class, AppConst.BENEFICE_MANAGER);

            if (config == null) {
                throw new ValueDataException("Aucune configuration de bénéfice du manager retrouvée");
            }

            if (membre == null) {
                throw new ValueDataException("Ce membre n'existe pas");
            }

            if (!membre.getLogin().getRole().toLowerCase().equals(type)) {
                throw new ValueDataException("Ce membre n'est pas un manager");
            }

            Compte compte = dao.getCompteMembre(membre.getId());
            Administrateur admin = (Administrateur) dao.findById(Administrateur.class, fkProvenance);

            if (admin == null) {
                throw new ValueDataException("Cet agent n'existe pas");
            }

            if (!admin.getRole().toLowerCase().equals("agent")) {
                throw new ValueDataException("Cet agent n'a pas le droit d'effectuer une approvisionnement");
            }

            if (compte == null) {
                throw new ValueDataException("Aucun compte retrouvé");
            }

            if (admin.getApproCDF() < montantcdf) {
                throw new ValueDataException("Votre solde CDF est insuffisant! Veuillez demander un approvisionnement");
            }
            admin.setApproCDF(admin.getApproCDF() - montantcdf);
            if (admin.getApproUSD() < montantusd) {
                throw new ValueDataException("Votre solde USD est insuffisant! Veuillez demander un approvisionnement");
            }
            admin.setApproUSD(admin.getApproUSD() - montantusd);

            if (!dao.saveOrUpdate(admin)) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            Double totalCDF = (montantcdf * config.getValueDecimal() / 100) + montantcdf;
            Double totalUSD = (montantusd * config.getValueDecimal() / 100) + montantusd;

            compte.setApproCDF(compte.getApproCDF() + totalCDF);
            compte.setApproUSD(compte.getApproUSD() + totalUSD);

            if (!dao.saveOrUpdate(compte)) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            JournalAppro journal = new JournalAppro();
            journal.setFkDestinateur(fkDestinateur);
            journal.setFkAgent(fkProvenance);
            journal.setMontantcdf(montantcdf);
            journal.setMontantusd(montantusd);
            journal.setType(type);
            journal.setStatut(true);
            journal.setDateCreat(new Date());
            journal.setSoldeCDF(compte.getApproCDF());
            journal.setSoldeUSD(compte.getApproUSD());
            journal.setBeneficeUSD((montantusd * config.getValueDecimal() / 100));
            journal.setBeneficeCDF((montantcdf * config.getValueDecimal() / 100));
            journal.setMontantTotalCDF(totalCDF);
            journal.setMontantTotalUSD(totalUSD);

            Integer id = dao.save(journal);
            if (id == null) {
                throw new ValueDataException("L'opération a échoué veuillez signaler à l'administration pour la correction");
            }

            done = true;
            this.error.setErrorDescription("Compte approvisionné avec succés avec succès");
            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return done;
    }

    private boolean accrediterCompteManager(Integer fkDestinateur, Double montantcdf, Double montantusd) {
        boolean done = false;
        String type = "manager";

        try {
            AppUtilities.controlValue(fkDestinateur, "veuillez renseigner le destinateur");
            //AppUtilities.controlValue(montantusd, "Veuillez renseigner le montant usd");
            //AppUtilities.controlValue(montantcdf, "Veuillez renseigner le montant cdf");

            DaoMembre dao = new DaoMembre();

            MembreAdapter membre = dao.getMembre(fkDestinateur);

            if (membre == null) {
                throw new ValueDataException("Ce membre n'existe pas");
            }

            if (!membre.getLogin().getRole().toLowerCase().equals(type)) {
                throw new ValueDataException("Ce membre n'est pas un manager");
            }

            Compte compte = dao.getCompteMembre(membre.getId());

            if (compte == null) {
                throw new ValueDataException("Aucun compte retrouvé");
            }

            compte.setApproCDF(compte.getApproCDF() + montantcdf);
            compte.setApproUSD(compte.getApproUSD() + montantusd);

            if (!dao.saveOrUpdate(compte)) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            done = true;
            this.error.setErrorDescription("Compte accredité avec succés avec succès");
            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return done;
    }

    public boolean approvisionnementAgence(String fkDestinateur, Integer fkProvenance, Double montantcdf, Double montantusd) {
        boolean done = false;
        String type = "agence";

        try {
            AppUtilities.controlValue(fkDestinateur, "veuillez renseigner le destinateur");
            AppUtilities.controlValue(fkProvenance, "veuillez renseigner l'agent MLI");
            //AppUtilities.controlValue(montantusd, "Veuillez renseigner le montant usd");
            //AppUtilities.controlValue(montantcdf, "Veuillez renseigner le montant cdf");

            DaoMembre dao = new DaoMembre();
            Administrateur provenance = (Administrateur) dao.findById(Administrateur.class, fkProvenance);
            Administrateur destination = dao.getAdmin(fkDestinateur);

            if (provenance == null) {
                throw new ValueDataException("Cet agent n'existe pas");
            }

            Logger.printLog("CTRL", provenance.getRole());

            if (!provenance.getRole().toLowerCase().equals("admin")) {
                throw new ValueDataException("Cet agent n'a pas le droit d'effectuer cette action");
            }
            if (destination == null) {
                throw new ValueDataException("L'agent de destination n'existe pas");
            }

            destination.setApproCDF(destination.getApproCDF() + montantcdf);
            destination.setApproUSD(destination.getApproUSD() + montantusd);

            if (!dao.saveOrUpdate(destination)) {
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            }

            JournalAppro journal = new JournalAppro();
            journal.setFkAgentDestinateur(destination.getId());
            journal.setFkAgent(fkProvenance);
            journal.setMontantcdf(montantcdf);
            journal.setMontantusd(montantusd);
            journal.setType(type);
            journal.setStatut(true);
            journal.setDateCreat(new Date());
            journal.setSoldeCDF(destination.getApproCDF());
            journal.setSoldeUSD(destination.getApproUSD());
            journal.setBeneficeUSD(0.0);
            journal.setBeneficeCDF(0.0);
            journal.setMontantTotalCDF(montantcdf);
            journal.setMontantTotalUSD(montantusd);

            Integer id = dao.save(journal);
            if (id == null) {
                throw new ValueDataException("L'opération a échoué veuillez signaler à l'administration pour la correction");
            }

            done = true;
            this.error.setErrorDescription("Compte approvisionné avec succés avec succès");
            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return done;
    }

    public RapportAdapter getRapportSystemeParrainage(String debut, String fin) {
        RapportAdapter rapport = null;

        try {
            AppUtilities.controlValue(debut, "Veuillez renseigner la date debut");

            rapport = new RapportAdapter();

            rapport.setMembres(this.getMembres(debut, fin));
            rapport.setParrainages(this.getListParrainageParDate(debut, fin));
            rapport.setPaiements(this.getPaiementForPeriode(debut, fin));

            this.error.setErrorCode("OK");

        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return rapport;
    }

    public List<Membre> getAllMembre() {
        List<Membre> list = null;

        try {

            DaoMembre dao = new DaoMembre();

            list = dao.getList(Membre.class);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<MembreAdapter> getMembres(String dateDebut, String dateFin) {
        List<MembreAdapter> list = null;

        try {
            AppUtilities.controlValue(dateDebut, "Veuillez saisir le mot clef");

            DaoMembre dao = new DaoMembre();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date debut = sdf.parse(dateDebut);

            Date fin = null;

            try {
                fin = sdf.parse(dateFin);

                if (fin == null) {
                    fin = debut;
                }

                fin = utilities.Utilities.transformEndDay(fin);

            } catch (Exception e) {
            }

            list = dao.getListMembre(debut, fin);

            if (list == null || list.size() <= 0) {
                throw new ValueDataException("Aucun membre trouvé");
            }

            this.error.setErrorCode(ErrorResponse.OK);
            this.error.setErrorDescription(list.size() + " Membre trouvés");

        } catch (ValueDataException e) {
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode(ErrorResponse.KO);
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<JournalAppro> getJournalApproForDestinateur(String dateDebut, String dateFin, Integer fk) {
        List<JournalAppro> list = null;

        try {
            AppUtilities.controlValue(dateDebut, "Veuillez renseigner une date valide");
            AppUtilities.controlValue(fk, "Veuiller renseigner l'identifiant");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date debut = sdf.parse(dateDebut), fin = null;

            try {
                fin = sdf.parse(dateFin);
                if (fin == null) {
                    fin = debut;
                }
                fin = utilities.Utilities.transformEndDay(fin);
            } catch (Exception e) {
            }

            DaoMembre dao = new DaoMembre();

            list = dao.getJournalApproForDestinateur(fk, debut, fin);

            if (list == null || list.size() < 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<JournalAppro> getJournalApproForProvenanceMembre(String dateDebut, String dateFin, Integer fk) {
        List<JournalAppro> list = null;

        try {
            AppUtilities.controlValue(dateDebut, "Veuillez renseigner une date valide");
            AppUtilities.controlValue(fk, "Veuiller renseigner l'identifiant");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date debut = sdf.parse(dateDebut), fin = null;

            try {
                fin = sdf.parse(dateFin);
                if (fin == null) {
                    fin = debut;
                }
                fin = utilities.Utilities.transformEndDay(fin);
            } catch (Exception e) {
            }

            DaoMembre dao = new DaoMembre();

            list = dao.getJournalApproForProvenanceMembre(fk, debut, fin);

            if (list == null || list.size() < 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<JournalAppro> getJournalApproForProvenanceAgent(String dateDebut, String dateFin, Integer fk) {
        List<JournalAppro> list = null;

        try {
            AppUtilities.controlValue(dateDebut, "Veuillez renseigner une date valide");
            AppUtilities.controlValue(fk, "Veuiller renseigner l'identifiant");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date debut = sdf.parse(dateDebut), fin = null;

            try {
                fin = sdf.parse(dateFin);
                if (fin == null) {
                    fin = debut;
                }
                fin = utilities.Utilities.transformEndDay(fin);
            } catch (Exception e) {
            }

            DaoMembre dao = new DaoMembre();

            list = dao.getJournalApproForProvenanceAgent(fk, debut, fin);

            if (list == null || list.size() < 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }

    public List<JournalAppro> getJournalAppro(String dateDebut, String dateFin, String type) {
        List<JournalAppro> list = null;

        try {
            AppUtilities.controlValue(dateDebut, "Veuillez renseigner une date valide");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            Date debut = sdf.parse(dateDebut), fin = null;

            try {
                fin = sdf.parse(dateFin);
                if (fin == null) {
                    fin = debut;
                }
                fin = utilities.Utilities.transformEndDay(fin);
            } catch (Exception e) {
            }

            DaoMembre dao = new DaoMembre();

            list = dao.getJournalAppro(debut, fin, type);

            if (list == null || list.size() < 0) {
                throw new ValueDataException(AppConst.EMPTY_LIST);
            }

            this.error.setErrorCode("OK");

        } catch (ValueDataException e) {
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            this.error.setErrorCode("KO");
            this.error.setErrorDescription(AppConst.ERROR_UNKNOW);
        }

        return list;
    }
}
