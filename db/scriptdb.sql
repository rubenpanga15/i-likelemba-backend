create table `membre`(
    id int(11) unsigned auto_increment primary key,
    nom varchar(255) not null,
    postnom varchar(255) not null,
    prenom varchar(255) not null,
    sexe varchar(1) not null,
    email varchar(255),
    telephone varchar(255),
    fkParrain int(11),
    fkArbre varchar(25),
    lieu varchar(255),
    dateNaissance date not null,
    statut tinyint(1) default 0,
    dateCreat datetime default now()
);

create table `login`(
    id int(11) unsigned auto_increment primary key,
    username varchar(255) not null,
    `password` varchar(255) not null,
    token varchar(1000) not null,
    fkMembre int(11) unsigned not null,
	isFirstConnection tinyint(1) default 0,
    statut tinyint(1) default 1,
    dateCreat datetime default now()
);

create table `compte`(
    id int(11) unsigned primary key,
    fkMembre int(11) unsigned not null,
    soldeUSD decimal(11, 6) not null,
    soldeCDF decimal(11, 6) not null,
    statut tinyint(1) default 1,
    dateCreat datetime default now()
);

create table `mouvementCompte`(
    id int(11) unsigned auto_increment primary key,
    fkCompte int(11) unsigned not null,
    fkMembre int(11) unsigned not null,
    debitUSD decimal(11, 6) not null default 0,
    creditUSD decimal(11, 6) not null default 0,
    soldeUSD decimal(11, 6) not null default 0,
    debitCDF decimal(11, 6) not null default 0,
    creditCDF decimal(11, 6) not null default 0,
    soldeCDF decimal(11, 6) not null default 0,
	description varchar(11,6) not null,
    statut tinyint(1) default 1,
    dateCreat datetime default now()
);

create table `arbre`(
    code varchar(25) primary key,
    fkGeniteur int(11) not null,
	description varchar(255) not null,
    fkCategorieAdhesion int(11) ,
    fkPartage int(11) not null,
    statut tinyint(1) default 1,
    dateCreat datetime default now()
);

create table `categorieAdhesion`(
    id int(11) unsigned auto_increment primary key,
	fkDevise int(11) not null,
    `description` varchar(255) not null,
    montantAdhesion decimal(11, 3) not null,
    statut tinyint(1) default 1,
    dateCreat datetime default now()
);

create table `administrateur`(
    id int(11) unsigned auto_increment primary key,
    code varchar(8) not null,
    noms varchar(255) not null,
    username varchar(255) not null,
    `password` varchar(255) not null,
    token varchar(255) not null,
    fkProfilAcces int(11) unsigned not null,
    statut tinyint(1) default 1,
    dateCreat datetime default now()
);

create table `paiement`(
    id int(11) unsigned auto_increment primary key,
    fkMembre int(11) unsigned not null,
    fkAdmin varchar(25) not null,
    montantCDF decimal(11, 3) not null,
    montantUSD decimal(11, 3) not null,
    dateDemande datetime not null,
    datePaiement datetime,
    etat varchar(25) not null default 'ATTENTE'
    statut tinyint(1) not null default 0,
    dateCreat datetime
);

create table `profilAcces`(
    id int(11) unsigned auto_increment primary key,
    `description` varchar(255) not null,
    statut tinyint(1) default 1,
    dateCreat datetime default now()
);

create table `devise`(
	id int(11) unsigned auto_increment primary key,
	code varchar(5) not null,
	description varchar(255) not null,
	statut tinyint(1) default 1,
	dateCreat datetime default now()
);

create table `partage`(
    id int(11) unsigned auto_increment primary key,
    g1 decimal(11,6) not null,
    g2 decimal(11,6) not null,
    g3 decimal(11, 6) not null,
    statut tinyint(1) DEFAULT 1,
    dateCreat DATETIME default now()
);

create table `parrainage`(
	id int(11) unsigned auto_increment primary key,
	fkMembre int(11) not null,
	fkParrain int(11),
	fkArbre varchar(25),
	statut tinyint(1) not null default 0,
	dateCreat datetime not null default now(),
	fkAgent int(11)
);

create table `journaloperation` (
	id int(11) unsigned auto_increment primary key,
	fkParrainage int(11) not null,
	fkMembre int(11) not null,
	fkArbre varchar(11) not null,
	statut tinyint(1) not default 1,
	dateCreat datetime default now()
)

