create procedure add_filleul(
    p_nom varchar(255),
    p_postnom varchar(255),
    p_prenom varchar(255),
    p_telephone varchar(255),
    p_email varchar(255),
    p_sexe varchar(1)
    p_fkParrain int(11),
    p_fkArbre varchar(255),
    p_password varchar(255)
)
begin
    declare membreId int(11);
    set AUTOCOMMIT = 0;

    -- Enregistrement membre
    insert into membre(nom, postnom, prenom, telephone,
    sexe, fkParrain, fkArbre) values (
        aes_encrypt(p_nom, 'ruben78236**'), aes_encrypt(p_postnom, 'ruben78236**'), aes_encrypt(p_prenom, 'ruben78236**'), p_telephone, p_email,
        p_sexe, p_fkParrain, p_fkArbre
    );

    -- Recuperation de l'identifiant
    select id into membreId from Membre where
        nom = aes_encrypt(p_nom, 'ruben78236**') and 
        prenom= aes_encrypt(p_prenom, 'ruben78236**') and 
        telephone = p_telephone order by dateCtreat desc limit 1;

    -- Enregistrement login
    if membreId is not null then 
        insert into `login`(username, password, token, fkMembre) values(
        aes_encrypt(p_telephone, 'ruben78236**'), aes_encrypt(p_password, 'ruben78236**'), '', membreId
        );

        -- Creation compte
        insert into compte (fkMembre, soldeUSD, soldeCDF) values(membreId, 0, 0);
        COMMIT;
    end if;

    select membreId;


end;

create procedure create_arbre(
	p_fkGeniteur int(11),
	p_fkPartage int(11),
	p_montant decimal(11, 6),
	p_description varchar(255),
	p_fkDevise int(11)
)
begin
	set autocommit = 1;
	insert into arbre(fkGeniteur, fkPartage, montant, description, fkDevise, statut) values (p_fkGeniteur, p_fkPartage, p_montant, p_description, p_fkDevise, 0);
	select code, fkGeniteur, fkPartage, montant, description, fkDevise from Arbre where fkGeniteur = p_fkGeniteur and description = p_description order by dateCreat desc limit 1;
	
end;