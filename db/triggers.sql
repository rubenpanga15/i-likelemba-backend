create trigger 'tr_create_compte' on membre after insert each row
begin
    insert into compte(fkMembre, soldeUSD, soldeCDF, statut) values (new.id, 0,0,1); 
end;

create trigger `tr_arbre` on arbre before insert each row
begin
	declare v_nom varchar(255);
	declare v_prenom varchar(255);
	declare v_id int(11);
	declare nbr int(11) default 0;
	
	select upper(left(nom, 1)) into v_nom from Membre where id = new.fkGeniteur;
	select upper(left(prenom, 1)) into v_prenom from Membre where id = new.fkGeniteur;
	select id into v_id from Membre where id = new.fkGeniteur;
	select count(*) into nbr from arbre where fkGeniteur = new.fkGeniteur;
	
	if v_nom = '' or v_nom is null then
		set v_nom = '0';
	end if;
	if v_prenom = '' or v_prenom is null then
		set v_prenom = '0';
	end if;
	if nbr = '' or nbr is null then
		set nbr = 0;
	end if;
	set nbr = nbr + 1;
	
	set new.code = lpad(concat(v_nom, concat(v_prenom, concat(v_id, lpad(nbr,3,'0')))), 8, '0');
	
end;

create trigger 'tr_integration' on membre after insert each row
begin
	declare c_credit varchar(255) default 'CREDIT';
	declare c_debit varchar(255) default 'DEBIT';

	declare v_parrain, v_parrain_1, v_parrain_2 int(11);
	declare v_soldeUSD_parrain, v_soldeCDF_parrain decimal(11,6);
	declare v_soldeUSD_parrain_1, v_soldeCDF_parrain_1 decimal(11,6);
	declare v_soldeUSD_parrain_2, v_soldeCDF_parrain_2 decimal(11,6);
	
	declare v_compte_parrain, v_compte_parrain_1,v_compte_parrain_2 int(11); 
	
	declare v_id_arbre varchar(25);
	declare v_id_partage, v_id_devise int(11);
	declare partage_g1, partage_g2, partage_g3, montantAdhesion decimal(11,6) default 0;
	
	declare nbr int(3) default 0;
	
	declare montant_parrain_cdf, montant_parrain_usd decimal(11, 6) default 0;
	declare montant_parrain_cdf_1, montant_parrain_usd_1 decimal(11, 6) default 0;
	declare montant_parrain_cdf_2, montant_parrain_usd_2 decimal(11, 6) default 0;
	
	-- recuperation de l'id du parrain, arbre et partage
	select fkParrain, fkArbre into v_parrain, v_id_arbre from parrainage where fkMembre = new.id;
	select fkPartage, montant, fkDevise into v_id_partage, montantAdhesion, v_id_devise from arbre where code = v_id_arbre;
	select g1, g2, g3 into partage_g1, partage_g2, partage_g3 from partage where id = v_id_partage;
	
	if v_parrain is not null then
		-- recuperation des nombres filleuls actifs pour le parrain
		select count(*) into nbr from parrainage where fkParrain = v_parrain and fkArbre = v_id_arbre and statut > 0;
		
		if nbr is not null and nbr = 5 then
			-- Mise à jour du compte du parrain
			select id, soldeUSD, soldeCDF into v_compte_parrain, v_soldeUSD_parrain, v_soldeCDF_parrain from compte where fkMembre = v_parrain;
			
			if v_id_devise = 1 then
				set montant_parrain_cdf = montantAdhesion * partage_g1 * 5;
				set v_soldeCDF_parrain = v_soldeCDF_parrain + (montantAdhesion * partage_g1* 5);
			end if;
			if v_id_devise = 2 then
				set montant_parrain_usd = montantAdhesion * partage_g1 * 5;
				set v_soldeUSD_parrain = v_soldeUSD_parrain + (montantAdhesion * partage_g1 * 5);
			end if;
			update compte set soldeCDF = v_soldeCDF_parrain, soldeUSD = v_soldeUSD_parrain where fkMembre = v_parrain;
			insert into mouvementCompte(fkCompte, fkMembre, creditCDF, soldeCDF, creditUSD, soldeUSD, description) values (v_compte_parrain, v_parrain, montant_parrain_cdf, v_soldeCDF_parrain, montant_parrain_usd, v_soldeUSD_parrain, c_credit);
			
			-- recuperation de l'id du parrain 1 et traitement
			-- select fkParrain into v_parrain_1 from membre where id = v_parrain;
			select fkParrain into v_parrain_1 from parrainage where fkMembre = v_parrain and fkArbre = v_id_arbre;
			if v_parrain_1 is not null then
				select id, soldeUSD, soldeCDF into v_compte_parrain_1, v_soldeUSD_parrain_1, v_soldeCDF_parrain_1 from compte where fkMembre = v_parrain_1;
				if v_id_devise = 1 then
					set montant_parrain_cdf_1 = montantAdhesion * partage_g2;
					set v_soldeCDF_parrain_1 = v_soldeCDF_parrain_1 + (montantAdhesion * partage_g2);
				end if;
				if v_id_devise = 2 then
					set montant_parrain_usd_1 = montantAdhesion * partage_g2;
					set v_soldeUSD_parrain_1 = v_soldeUSD_parrain_1+ (montantAdhesion * partage_g2);
				end if;
				update compte set soldeCDF = v_soldeCDF_parrain_1, soldeUSD = v_soldeUSD_parrain_1 where fkMembre = v_parrain_1;
				insert into mouvementCompte(fkCompte, fkMembre, creditCDF, soldeCDF, creditUSD, soldeUSD, description) values (v_compte_parrain_1, v_parrain_1, montant_parrain_cdf_1, v_soldeCDF_parrain_1, montant_parrain_usd_1, v_soldeUSD_parrain_1, c_credit);
				
				-- Recuperation de l'id du parrain 2 et traitement
				-- select fkParrain into v_parrain_2 from membre where id = v_parrain_1;
				select fkParrain into v_parrain_2 from parrainage where fkMembre = v_parrain_2 and fkArbre = v_id_arbre;
				if v_parrain_2 is not null then
					select id, soldeUSD, soldeCDF into v_compte_parrain_2, v_soldeUSD_parrain_2, v_soldeCDF_parrain_2 from compte where fkMembre = v_parrain_2;
					if v_id_devise = 1 then
						set montant_parrain_cdf_2 = montantAdhesion * partage_g3;
						set v_soldeCDF_parrain = v_soldeCDF_parrain_2 + (montantAdhesion * partage_g3);
					end if;
					if v_id_devise = 2 then
						set montant_parrain_usd_2 = montantAdhesion * partage_g3;
						set v_soldeUSD_parrain_2 = v_soldeUSD_parrain_2+ (montantAdhesion * partage_g3);
					end if;
					update compte set soldeCDF = v_soldeCDF_parrain_2, soldeUSD = v_soldeUSD_parrain_2 where fkMembre = v_parrain_2;
					insert into mouvementCompte(fkCompte, fkMembre, creditCDF, soldeCDF, creditUSD, soldeUSD, description) values (v_compte_parrain_2, v_parrain_2, montant_parrain_cdf_2, v_soldeCDF_parrain_2, montant_parrain_usd_2, v_soldeUSD_parrain_2, c_credit);
				end if;
			end if;
		end if;
	end if;
	
end;

create trigger 'tr_xxx' on membre after insert each row
begin
	declare fk_parrain int(11)
	select fkParrain into fk_parrain from Membre where id = new.id;
end;

create trigger `tr_parrainge` after insert on `membre`each row
begin
	insert into parrainage(fkMembre, fkParrain, fkArbre) values (new.id, new.fkParrain, new.fkArbre);
end

create trigger `tr_admin` on arbre before insert each row
begin
	declare nbr int(11) default 0;
	
	select count(*) into nbr from administrateur;

	if nbr = 0 or nbr is null then
		set nbr = 1;
	end if;

	set new.code = concat('ADM', lpad(nbr, 7, '0'));
end